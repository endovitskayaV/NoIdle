package ru.vsu.noidle.state

import javafx.scene.input.KeyCode
import ru.vsu.noidle.model.KeyInfo
import ru.vsu.noidle.model.Language
import ru.vsu.noidle.model.LanguageInfo
import ru.vsu.noidle.util.Constants

data class Statistics(

        var projects: MutableSet<String> = mutableSetOf(),

        //--data to send to server--//
        var maxContinuousTimePerDay: Long = Constants.DEFAULT_LONG,
        var maxContinuousTimePerLife: Long = Constants.DEFAULT_LONG,
        var maxContinuousSymbolsPerDay: Long = Constants.DEFAULT_LONG,
        var maxContinuousSymbolsPerLife: Long = Constants.DEFAULT_LONG,
        var timePerDay: Long = Constants.DEFAULT_LONG,
        var timePerLife: Long = Constants.DEFAULT_LONG,
        var symbolsQuantityPerDay: Long = Constants.DEFAULT_LONG,
        var symbolsQuantityPerLife: Long = Constants.DEFAULT_LONG,
        var keyTapQuantity: MutableMap<String, KeyInfo> =
                KeyCode.values().map { it.getName() to KeyInfo() }.toMap().toMutableMap(),
        var successfulCommitsPerLife: Long = Constants.DEFAULT_LONG,
        var failedCommitsPerLife: Long = Constants.DEFAULT_LONG,
        var successfulCommitsPerDay: Long = Constants.DEFAULT_LONG,
        var failedCommitsPerDay: Long = Constants.DEFAULT_LONG,
        var successfulExecutionsPerLife: Long = Constants.DEFAULT_LONG,
        var failedExecutionsPerLife: Long = Constants.DEFAULT_LONG,
        var successfulExecutionsPerDay: Long = Constants.DEFAULT_LONG,
        var failedExecutionsPerDay: Long = Constants.DEFAULT_LONG,
        var languageInfo: MutableMap<String, LanguageInfo> =
                Language.values().map { it.shortcut to LanguageInfo(name = it) }.toMap().toMutableMap(),
        //----------//

        //local data
        var sentTimePerDay: Long = Constants.DEFAULT_LONG,
        var sentMaxContinuousTimePerDay: Long = Constants.DEFAULT_LONG
) {
    fun resetStateToNextDay() {
        sentTimePerDay = Constants.DEFAULT_LONG
        timePerDay = Constants.DEFAULT_LONG
        symbolsQuantityPerDay = Constants.DEFAULT_LONG
        successfulCommitsPerDay = Constants.DEFAULT_LONG
        failedCommitsPerDay = Constants.DEFAULT_LONG
        successfulExecutionsPerDay = Constants.DEFAULT_LONG
        failedExecutionsPerDay = Constants.DEFAULT_LONG
        maxContinuousTimePerDay = Constants.DEFAULT_LONG
        languageInfo.forEach { _, info ->
            info.timePerDay = Constants.DEFAULT_LONG
            info.symbolsPerDay = Constants.DEFAULT_LONG
        }
        keyTapQuantity.forEach { _, info ->
            info.perDay = Constants.DEFAULT_LONG
        }
    }
}