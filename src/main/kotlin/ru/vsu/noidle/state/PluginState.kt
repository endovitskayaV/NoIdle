package ru.vsu.noidle.state

import com.intellij.openapi.components.PersistentStateComponent
import com.intellij.openapi.components.ServiceManager
import com.intellij.openapi.components.State
import com.intellij.openapi.components.Storage
import com.intellij.openapi.project.Project
import com.intellij.util.xmlb.XmlSerializerUtil
import ru.vsu.noidle.model.Team
import ru.vsu.noidle.util.Constants
import ru.vsu.noidle.util.ProjectUtils

/**
 * Saves data locally at indicated storage
 *
 */
@State(name = Constants.STATE_COMPONENT_NAME, storages = [Storage(Constants.XML_NAME)])
data class PluginState(

        //data to send to server
        var apiKey: String = Constants.EMPTY_STRING,
        var teams: MutableMap<Team, Statistics> = mutableMapOf(Constants.DEFAULT_TEAM to Statistics(projects = mutableSetOf(Constants.DEFAULT_PROJECT_ID))),

        //local data
        var lastNotificationHours: Int = Constants.DEFAULT_INT,
        var notificationFrequencyHours: Int = Constants.DEFAULT_NOTIFICATION_H,
        var enabledAllNotifications: Boolean = Constants.DEFAULT_ENABLED_NOTIFICATIONS,
        var enabledLevelAchievementNotifications: Boolean = Constants.DEFAULT_ENABLED_NOTIFICATIONS,
        var enabledExtraAchievementNotifications: Boolean = Constants.DEFAULT_ENABLED_NOTIFICATIONS,
        var enabledTeamAchievementNotifications: Boolean = Constants.DEFAULT_ENABLED_NOTIFICATIONS,
        var enabledReportNotifications: Boolean = Constants.DEFAULT_ENABLED_NOTIFICATIONS,

        var scheme: String = Constants.DEFAULT_SCHEME,
        var host: String = Constants.DEFAULT_HOST,

        var today: Long = System.currentTimeMillis(),

        var previousFileExt: String = Constants.EMPTY_STRING,
        var email: String = Constants.EMPTY_STRING,
        var userName: String = Constants.EMPTY_STRING

) : PersistentStateComponent<PluginState> {

    override fun loadState(state: PluginState) {
        XmlSerializerUtil.copyBean(state, this)
    }

    override fun getState() = this

    fun resetStateToNextDay() {
        today = System.currentTimeMillis()
        lastNotificationHours = 0
        previousFileExt = Constants.EMPTY_STRING
        teams.forEach { _, statistics ->
            statistics.resetStateToNextDay()
        }
    }

    fun getTeamStatistics(projectPath: String?) =
            teams.entries.stream().filter { entry -> entry.value.projects.contains(projectPath) }.findFirst().orElse(null)

    fun getTeamByName(name: String?) =
            teams.entries.stream().filter { entry -> entry.key.name.equals(name) }.findFirst().orElse(null)?.key

    fun getTeamById(id: String?) =
            teams.entries.stream().filter { entry -> entry.key.id.equals(id) }.findFirst().orElse(null)?.key

    fun getTeamByProjectPath(projectPath: String?) =
            teams.entries.stream().filter { entry -> entry.value.projects.contains(projectPath) }.findFirst().orElse(null)?.key

    fun getStatistics(projectPath: String?) =
            teams.entries.stream().filter { entry -> entry.value.projects.contains(projectPath) }.findFirst().orElse(null)?.value

    fun teamSpecified(project: Project) = teams.entries.stream().anyMatch { entry ->
        entry.value.projects.contains(ProjectUtils.getProjectId(project)) && !entry.key.equals(Constants.DEFAULT_TEAM)
    }

    fun getTeamForCurrentProject(project: Project?) = teams.entries.stream()
            .filter { entry -> entry.value.projects.contains(ProjectUtils.getProjectId(project)) }.findFirst().orElse(null)?.key

    fun getOverallDefaultStatistics() = teams[Constants.DEFAULT_TEAM]!!

    companion object {
        @Volatile
        private var INSTANCE: PluginState? = null

        fun getInstance(): PluginState =
                INSTANCE ?: synchronized(this) {
                    INSTANCE ?: ServiceManager.getService(PluginState::class.java).also { INSTANCE = it }
                }
    }
}