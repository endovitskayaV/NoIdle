package ru.vsu.noidle.ui.dialog

import com.intellij.openapi.project.Project
import com.intellij.openapi.project.ProjectManager
import com.intellij.openapi.ui.DialogWrapper
import com.intellij.openapi.ui.MessageType
import com.intellij.openapi.ui.ValidationInfo
import com.intellij.openapi.ui.popup.Balloon
import com.intellij.openapi.ui.popup.JBPopupFactory
import com.intellij.ui.awt.RelativePoint
import ru.vsu.noidle.state.PluginState
import ru.vsu.noidle.util.Constants
import ru.vsu.noidle.util.StateUtils
import java.awt.Desktop
import java.awt.event.MouseEvent
import javax.swing.JComponent
import javax.swing.JPanel
import javax.swing.JTextField

/**
 * Dialog that allows to enter api key (user id)
 * Saves api key in state
 */
class ApiKeyDialog(var project: Project?) : DialogWrapper(project) {
    private var panel: JPanel = JPanel()
    private var textField: JTextField = JTextField(Constants.API_KEY_TEXT_FIELD_WIDTH)

    init {
        title = Constants.SETTINGS_DIALOG_NAME
        panel.add(textField)
        init()
    }

    override fun createCenterPanel(): JComponent? {
        return panel
    }

    override fun doValidate(): ValidationInfo? {
        val apiKey = textField.text

        //check if UUID was entered
        return if (StateUtils.getInstance().isValidUUID(apiKey))
            null
        else
            ValidationInfo(Constants.INVALID_API_KEY_MESSAGE)
    }

    override fun doOKAction() {
        PluginState.getInstance().apiKey = textField.text

        super.doOKAction() //close dialog

        Thread { StateUtils.getInstance().setEmail() }.start() //get email from server and save in state

        //show team dialog if only one project was opened (otherwise user set team in settings)
        // need this code because project opened event is called before this and no team dialog is displayed
        val openProjects = ProjectManager.getInstance().openProjects
        if (openProjects.size == 1 && !openProjects[0].equals(ProjectManager.getInstance().defaultProject)) {
            TeamDialog(openProjects[0]).show()
        }

    }

    override fun getHelpId(): String? {
        return Constants.HELP_ID_API_KEY_DIALOG
    }

    override fun doHelpAction() {
        JBPopupFactory.getInstance().createHtmlTextBalloonBuilder(Constants.HELP_TEXT_API_KEY_DIALOG, MessageType.INFO) {
            hyperlinkEvent ->
            if ((hyperlinkEvent.inputEvent as? MouseEvent)?.id?.equals(MouseEvent.MOUSE_CLICKED) == true) {
                Desktop.getDesktop().browse(hyperlinkEvent.url.toURI())
            }
        }
                .createBalloon()
                .show(RelativePoint.getSouthOf(panel), Balloon.Position.below);
    }
}