package ru.vsu.noidle.ui.dialog

import com.intellij.openapi.project.Project
import com.intellij.openapi.ui.DialogBuilder
import com.intellij.openapi.ui.DialogWrapper
import com.intellij.openapi.ui.MessageType
import com.intellij.openapi.ui.ValidationInfo
import com.intellij.openapi.ui.popup.Balloon
import com.intellij.openapi.ui.popup.JBPopupFactory
import com.intellij.ui.awt.RelativePoint
import ru.vsu.noidle.sender.Sender
import ru.vsu.noidle.state.PluginState
import ru.vsu.noidle.util.Constants
import ru.vsu.noidle.util.ProjectUtils
import ru.vsu.noidle.util.StateUtils
import java.awt.Desktop
import java.awt.event.MouseEvent
import java.util.*
import javax.swing.JComponent
import javax.swing.JLabel
import javax.swing.JPanel
import javax.swing.JTextField

/**
 * Dialog that allows to enter team id or name
 * Saves team name in state
 */
class TeamDialog(var project: Project?) : DialogWrapper(project) {
    private var rootPanel: JPanel = JPanel()
    private var textField: JTextField = JTextField(Constants.TEAM_TEXT_FIELD_WIDTH)

    init {
        val projName = if (project != null) "'" + project?.name + "'" else Constants.EMPTY_STRING
        title = Constants.TEAM_DIALOG_NAME + projName
        rootPanel.add(textField)
        init()
    }

    override fun createCenterPanel(): JComponent? {
        return rootPanel
    }

    override fun doValidate(): ValidationInfo? {
        return if (Sender.checkIfAddedUserTeam(UUID.fromString(PluginState.getInstance().apiKey), textField.text))
            null
        else
            ValidationInfo(Constants.INVALID_TEAM_NAME_MESSAGE)
    }

    override fun doCancelAction() {
        ProjectUtils.getProjectId(project)?.let { PluginState.getInstance().teams[Constants.DEFAULT_TEAM]?.projects?.add(it) }
        super.doCancelAction()
    }

    override fun doOKAction() {
        if (Sender.checkIfAddedUserTeam(UUID.fromString(PluginState.getInstance().apiKey), textField.text)) {
            StateUtils.getInstance().setTeamForCurrentProject(textField.text, project)
            super.doOKAction()
        } else {
            val errorDialogBuilder = DialogBuilder(project)
            errorDialogBuilder.setTitle(Constants.TEAM_DIALOG_NAME)
            val panel = JPanel()
            panel.add(JLabel(Constants.INVALID_TEAM_NAME_MESSAGE))
            errorDialogBuilder.setCenterPanel(panel)
            super.doOKAction() //close team dialog
            errorDialogBuilder.show()
        }
    }

    override fun getHelpId(): String? {
        return Constants.HELP_ID_TEAM_DIALOG
    }

    override fun doHelpAction() {
        JBPopupFactory.getInstance().createHtmlTextBalloonBuilder(
                Constants.HELP_TEXT_TEAM_DIALOG,
                MessageType.INFO) { hyperlinkEvent ->
            if ((hyperlinkEvent.inputEvent as? MouseEvent)?.id?.equals(MouseEvent.MOUSE_CLICKED) == true) {
                Desktop.getDesktop().browse(hyperlinkEvent.url.toURI())
            }
        }
                .createBalloon()
                .show(RelativePoint.getSouthOf(rootPanel), Balloon.Position.below);
    }
}