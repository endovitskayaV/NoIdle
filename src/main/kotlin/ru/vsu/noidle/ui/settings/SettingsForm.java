package ru.vsu.noidle.ui.settings;

import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.DialogBuilder;
import com.intellij.openapi.ui.DialogWrapper;
import org.jetbrains.annotations.NotNull;
import ru.vsu.noidle.model.Team;
import ru.vsu.noidle.sender.Sender;
import ru.vsu.noidle.state.PluginState;
import ru.vsu.noidle.util.Constants;
import ru.vsu.noidle.util.ProjectUtils;
import ru.vsu.noidle.util.StateUtils;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.net.URI;
import java.util.UUID;

/**
 * Plugin settings form
 * Is shown in File -> Settings -> Other Setting -> {@link Constants#PLUGIN_NAME}
 */
public class SettingsForm {
    private JPanel rootPanel;
    private JSpinner frequencySpinner;
    private JLabel hoursLabel;
    private JTextField apiKeyTextField;
    private JCheckBox enabledNotifications;
    private JCheckBox enabledReport;
    private JCheckBox enabledLevelAchievement;
    private JLabel invalidKeyLabel;
    private JSeparator notificationSeparator;
    private JPanel notificationPanel;
    private JTextField teamTextField;
    private JLabel invalidTeamLabel;
    private JLabel teamLabel;
    private JPanel generalPanel;
    private JCheckBox enabledExtraAchievement;
    private JCheckBox enabledTeamAchievement;
    private JPanel serverPanel;
    private JTextField hostTextField;
    private JTextField schemeTextField;
    private JLabel invalidUrlLabel;
    private JCheckBox defaultUrlCheckBox;
    private JLabel teamHelp;

    private PluginState state = PluginState.Companion.getInstance();

    private boolean isInit = true;

    public SettingsForm() {
        setMargins();

        teamHelp.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        teamHelp.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                try {
                    Desktop.getDesktop().browse(URI.create(Constants.INVALID_TEAM_LINK));
                } catch (IOException e1) {
                    System.out.println(e1.getMessage());
                }
            }
        });
    }

    public JPanel getRootPanel() {
        return rootPanel;
    }

    public boolean isModified() {
        hoursLabel.setText(((int) frequencySpinner.getValue()) == 1 ? Constants.HOUR : Constants.HOURS);

        if (isInit) {
            setTeams();
            isInit = false;
        }

        Team team = state.getTeamForCurrentProject(ProjectUtils.Companion.getProject(rootPanel));
        String teamName = team != null ? team.getName() : Constants.EMPTY_STRING;

        if (Constants.DEFAULT_TEAM_NAME.equals(teamName)) {
            teamName = Constants.EMPTY_STRING;
        }

        boolean isModified = false;
        isModified |= !(apiKeyTextField.getText().equals(state.getApiKey()));
        isModified |= !(teamTextField.getText().equals(teamName));
        isModified |= !(((int) frequencySpinner.getValue()) == state.getNotificationFrequencyHours());
        isModified |= !(enabledNotifications.isSelected() == state.getEnabledAllNotifications());
        isModified |= !(enabledLevelAchievement.isSelected() == state.getEnabledLevelAchievementNotifications());
        isModified |= !(enabledExtraAchievement.isSelected() == state.getEnabledExtraAchievementNotifications());
        isModified |= !(enabledTeamAchievement.isSelected() == state.getEnabledTeamAchievementNotifications());
        isModified |= !(enabledReport.isSelected() == state.getEnabledReportNotifications());

        if (!defaultUrlCheckBox.isSelected()) {
            isModified |= !(schemeTextField.getText().equals(state.getScheme()));
            isModified |= !(hostTextField.getText().equals(state.getHost()));
            invalidUrlLabel.setVisible(!isValidUrl());

            schemeTextField.setEnabled(true);
            hostTextField.setEnabled(true);
        } else {
            schemeTextField.setText(Constants.DEFAULT_SCHEME);
            hostTextField.setText(Constants.DEFAULT_HOST);
            schemeTextField.setEnabled(false);
            hostTextField.setEnabled(false);
            invalidUrlLabel.setVisible(false);
        }

        setNotificationsComponentsEnabled(enabledNotifications.isSelected());
        setReportNotificationsComponentsEnabled(enabledReport.isSelected());
        setTeamComponentsEnabled(isValidApiKey(state.getApiKey()));

        invalidKeyLabel.setVisible(!isValidApiKey(apiKeyTextField.getText()));

        if (teamTextField.getText() == null || teamTextField.getText().isEmpty()) {
            invalidTeamLabel.setText(Constants.DEFAULT_TEAM_SET_MESSAGE);
            invalidTeamLabel.setForeground(Constants.Companion.getOK_COLOR());
            invalidTeamLabel.setVisible(isValidApiKey(state.getApiKey()) && ProjectUtils.Companion.getProject(rootPanel) != null);
            teamHelp.setVisible(false);
        } else {
            boolean added = Sender.Companion.checkIfAddedUserTeam(UUID.fromString(PluginState.Companion.getInstance().getApiKey()), teamTextField.getText());
            if (!added) {
                invalidTeamLabel.setText(Constants.INVALID_TEAM_NAME_MESSAGE);
                invalidTeamLabel.setForeground(Constants.Companion.getERROR_COLOR());
                invalidTeamLabel.setVisible(isValidApiKey(state.getApiKey()) && ProjectUtils.Companion.getProject(rootPanel) != null);
                teamHelp.setVisible(isValidApiKey(state.getApiKey()) && ProjectUtils.Companion.getProject(rootPanel) != null);
            } else {//added
                invalidTeamLabel.setVisible(false);
                teamHelp.setVisible(false);
            }
        }

        return isModified;
    }

    public void apply() {
        if (!isValidApiKey(apiKeyTextField.getText())) {
            apiKeyTextField.setText(Constants.EMPTY_STRING);
            setTeamComponentsEnabled(isValidApiKey(state.getApiKey()));

            DialogBuilder apiKeyConfirmDialogBuilder = new DialogBuilder(rootPanel);
            apiKeyConfirmDialogBuilder.setTitle(Constants.SETTINGS_DIALOG_NAME);
            JPanel panel = new JPanel();
            panel.add(new JLabel(Constants.API_KEY_CONFIRM_MESSAGE));
            apiKeyConfirmDialogBuilder.setCenterPanel(panel);
            if (apiKeyConfirmDialogBuilder.show() == DialogWrapper.OK_EXIT_CODE) {
                doApply();
            }
        } else {
            doApply();
        }
    }

    /**
     * Saves data in state
     */
    private void doApply() {
        state.setApiKey(apiKeyTextField.getText());
        StateUtils.Companion.getInstance().setTeamForCurrentProject(teamTextField.getText(), ProjectUtils.Companion.getProject(rootPanel));
        StateUtils.Companion.getInstance().setEmail();

        //only integers are allowed by default - no need to check
        state.setNotificationFrequencyHours((int) frequencySpinner.getValue());

        state.setEnabledAllNotifications(enabledNotifications.isSelected());
        state.setEnabledReportNotifications(enabledReport.isSelected());
        state.setEnabledLevelAchievementNotifications(enabledLevelAchievement.isSelected());
        state.setEnabledExtraAchievementNotifications(enabledExtraAchievement.isSelected());
        state.setEnabledTeamAchievementNotifications(enabledTeamAchievement.isSelected());

        if (isValidUrl()) {
            state.setScheme(schemeTextField.getText());
            state.setHost(hostTextField.getText());
        } else {
            schemeTextField.setText(Constants.DEFAULT_SCHEME);
            hostTextField.setText(Constants.DEFAULT_HOST);
            state.setScheme(Constants.DEFAULT_SCHEME);
            state.setHost(Constants.DEFAULT_HOST);
        }

    }

    public void reset() {
        setValues();
    }

    private void setNotificationsComponentsEnabled(boolean enabled) {
        //set enability
        enabledLevelAchievement.setEnabled(enabled);
        enabledExtraAchievement.setEnabled(enabled);
        enabledTeamAchievement.setEnabled(enabled);
        enabledReport.setEnabled(enabled);
        notificationSeparator.setEnabled(enabled);
        frequencySpinner.setEnabled(enabled);
        hoursLabel.setEnabled(enabled);

        if (!enabled) { //remove selection
            enabledLevelAchievement.setSelected(false);
            enabledExtraAchievement.setSelected(false);
            enabledTeamAchievement.setSelected(false);
            enabledReport.setSelected(false);
            setReportNotificationsComponentsEnabled(false);
        }
    }

    private void setReportNotificationsComponentsEnabled(boolean enabled) {
        frequencySpinner.setEnabled(enabled);
        hoursLabel.setEnabled(enabled);
    }

    private void setTeamComponentsEnabled(boolean enabled) {
        teamTextField.setEnabled(enabled);
        teamLabel.setEnabled(enabled);
        if (!enabled) {
            setInvalid(invalidTeamLabel, Constants.INVALID_TEAM_NAME_MESSAGE);
        }
    }

    /**
     * @return whether UUID was entered
     */
    private boolean isValidApiKey(String key) {
        return StateUtils.Companion.getInstance().isValidUUID(key);
    }

    private boolean isValidUrl() {
        return StateUtils.Companion.getInstance().isValidUrl(schemeTextField.getText(), hostTextField.getText());
    }

    private void setMargins() {
        notificationPanel.setBorder(Constants.Companion.getMIN_SECTION_MARGIN());
    }

    private void setValues() {
        //-----------------------general----------------------------//
        apiKeyTextField.setText(state.getApiKey());
        setInvalid(invalidKeyLabel, Constants.INVALID_API_KEY_MESSAGE);

        //---------------------------notifications------------------------------//
        frequencySpinner.setModel(new SpinnerNumberModel(
                state.getNotificationFrequencyHours(), Constants.MIN_NOTIFICATION_H, Constants.MAX_NOTIFICATION_H, Constants.STEP_NOTIFICATION_H));

        enabledReport.setSelected(state.getEnabledReportNotifications());
        //  setReportNotificationsComponentsEnabled(enabledReport.isSelected());

        enabledLevelAchievement.setSelected(state.getEnabledLevelAchievementNotifications());
        enabledExtraAchievement.setSelected(state.getEnabledExtraAchievementNotifications());
        enabledTeamAchievement.setSelected(state.getEnabledTeamAchievementNotifications());

        enabledNotifications.setSelected(state.getEnabledAllNotifications());
        setNotificationsComponentsEnabled(enabledNotifications.isSelected());

        //----------------------------server--------------------------------//
        schemeTextField.setText(state.getScheme());
        hostTextField.setText(state.getHost());

        defaultUrlCheckBox.setSelected((state.getScheme().equals(Constants.DEFAULT_SCHEME) && state.getHost().equals(Constants.DEFAULT_HOST)));
        schemeTextField.setEnabled(defaultUrlCheckBox.isSelected());
        hostTextField.setEnabled(defaultUrlCheckBox.isSelected());

        setInvalid(invalidUrlLabel, Constants.INVALID_URL_MESSAGE);
    }

    private void setInvalid(@NotNull JLabel invalidTextField, String message) {
        invalidTextField.setText(message);
        invalidTextField.setForeground(Constants.Companion.getERROR_COLOR());
        invalidTextField.setVisible(false);
    }

    private void setTeams() {
        Project project = ProjectUtils.Companion.getProject(rootPanel);
        if (project == null) {
            teamLabel.setVisible(false);
            teamTextField.setVisible(false);
            invalidTeamLabel.setVisible(false);
        } else {
            checkTeam();

            String id = ProjectUtils.Companion.getProjectId(project);
            Team team = state.getTeamByProjectPath(id);
            if (team != null) {
                teamTextField.setText(team.getName());
            }
            setTeamComponentsEnabled(isValidApiKey(state.getApiKey()));
        }
    }

    private void checkTeam() {
        String id = ProjectUtils.Companion.getProjectId(rootPanel);
        Team team = state.getTeamByProjectPath(id);
        if (team != null) {
            Team serverTeam = Sender.Companion.getTeamByName(team.getName());
            if (serverTeam == null) {
                Sender.Companion.resetName(team);
            }
        }

    }
}
