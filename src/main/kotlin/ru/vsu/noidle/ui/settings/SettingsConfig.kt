package ru.vsu.noidle.ui.settings

import com.intellij.openapi.options.SearchableConfigurable
import ru.vsu.noidle.util.Constants
import javax.swing.JComponent

/**
 * Manages [SettingsForm] in File -> Settings -> Other Setting -> [Constants.PLUGIN_NAME]
 */
class SettingsConfig : SearchableConfigurable {
    private lateinit var settingsForm: SettingsForm

    /**
     * Creates form
     */
    override fun createComponent(): JComponent? {
        settingsForm = SettingsForm()
        return settingsForm.rootPanel
    }

    //--------Invokes on form creating---------//
    override fun getId(): String {
        return Constants.PLUGIN_NAME;
    }

    override fun getDisplayName(): String {
        return Constants.PLUGIN_NAME;
    }
    //--------------------------------------//

    /**
     * Check if data in form was modified to enable Ok / Apply / Reset buttons
     */
    override fun isModified(): Boolean {
        return settingsForm.isModified
    }

    /**
     * Invokes on Ok / Apply buttons click
     */
    override fun apply() {
        settingsForm.apply()
    }

    /**
     * Invokes on Reset / Cancel / Exit without save click
     */
    override fun reset() {
        settingsForm.reset()
    }
}