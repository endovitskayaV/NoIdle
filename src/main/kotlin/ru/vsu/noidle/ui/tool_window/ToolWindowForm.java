package ru.vsu.noidle.ui.tool_window;

import com.intellij.openapi.progress.ProgressIndicator;
import com.intellij.openapi.progress.ProgressManager;
import com.intellij.openapi.progress.Task;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.popup.Balloon;
import com.intellij.openapi.util.IconLoader;
import com.intellij.openapi.wm.ToolWindow;
import com.intellij.openapi.wm.ToolWindowFactory;
import com.intellij.ui.content.ContentFactory;
import org.jetbrains.annotations.NotNull;
import ru.vsu.noidle.model.ContinuousType;
import ru.vsu.noidle.model.KeyInfo;
import ru.vsu.noidle.model.LanguageInfo;
import ru.vsu.noidle.model.Team;
import ru.vsu.noidle.sender.GetUser;
import ru.vsu.noidle.sender.SendStatisticsGetNotifications;
import ru.vsu.noidle.state.PluginState;
import ru.vsu.noidle.state.Statistics;
import ru.vsu.noidle.util.Constants;
import ru.vsu.noidle.util.ProjectUtils;
import ru.vsu.noidle.util.StateUtils;
import ru.vsu.noidle.util.TimeUtils;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ToolWindowForm implements ToolWindowFactory {
    private JPanel rootPanel;
    private JLabel timePerDayValue;
    private JLabel symbolsPerDayValue;
    private JLabel symbolsPerLifeValue;
    private JLabel timePerLifeValue;
    private JLabel successfulCommitsPerLifeValue;
    private JLabel singleKeysValuePerLife1;
    private JLabel maxContinuousTimePerDay;
    private JLabel maxContinuousSymbolsPerDay;
    private JLabel singleKeysValuePerLife2;
    private JLabel singleKeysValuePerLife3;
    private JLabel singleKeysValuePerLife5;
    private JLabel maxContinuousTimePerLife;
    private JLabel maxContinuousSymbolsPerLife;
    private JPanel toolbar;
    private JLabel failedCommitsPerLifeValue;
    private JLabel singleKeysValuePerLife4;
    private JLabel downloadLabel;
    private JLabel refreshLabel;
    private JPanel downloadPanel;
    private JPanel refreshPanel;
    private JPanel langPerDayPanel;
    private JLabel langTimePerDay1;
    private JLabel langSymbolPerDay1;
    private JLabel langTimePerDay2;
    private JLabel langSymbolPerDay2;
    private JLabel langPerDaySymbolsLabel;
    private JLabel langPerDayName1;
    private JLabel langPerDayName2;
    private JLabel languagesPerDayLabel;
    private JLabel langPerDayNameLabel;
    private JLabel langPerDayTimeLabel;
    private JLabel langPerDayName3;
    private JLabel langTimePerDay3;
    private JLabel langSymbolPerDay3;
    private JLabel langPerDayName4;
    private JLabel langPerDayName5;
    private JLabel langTimePerDay4;
    private JLabel langTimePerDay5;
    private JLabel langSymbolPerDay4;
    private JLabel langSymbolPerDay5;
    private JLabel languagesPerLifeLabel;
    private JLabel langPerLifeNameLabel;
    private JLabel langPerLifeTimeLabel;
    private JPanel langPerLifePanel;
    private JLabel langPerLifeName1;
    private JLabel langPerLifeName2;
    private JLabel langPerLifeName3;
    private JLabel langPerLifeName4;
    private JLabel langPerLifeName5;
    private JLabel langTimePerLife1;
    private JLabel langTimePerLife2;
    private JLabel langTimePerLife3;
    private JLabel langTimePerLife4;
    private JLabel langTimePerLife5;
    private JLabel langSymbolPerLife1;
    private JLabel langSymbolPerLife2;
    private JLabel langSymbolPerLife3;
    private JLabel langSymbolPerLife4;
    private JLabel langSymbolPerLife5;
    private JLabel langPerLifeSymbolsLabel;
    private JLabel timeTodayLabel;
    private JLabel symbolsTodayLabel;
    private JLabel timeOverallLabel;
    private JLabel symbolsOverallLabel;
    private JLabel commitsPerLifeLabel;
    private JLabel topKeysPerLifeLabel;
    private JLabel continuousTodayLabel;
    private JLabel maxTodayTimeLabel;
    private JLabel maxTodaySymbolsLabel;
    private JLabel continuousOverallLabel;
    private JLabel maxOverallTimeLabel;
    private JLabel maxOverallSymbolsLabel;
    private JPanel dataPerDayPanel;
    private JLabel successfulExecutionsPerLifeValue;
    private JLabel failedExecutionsPerLifeValue;
    private JLabel commitsPerDayLabel;
    private JLabel successfulCommitsPerDayValue;
    private JLabel failedCommitsPerDayValue;
    private JLabel failedExecutionsPerDayValue;
    private JLabel successfulExecutionsPerDayValue;
    private JLabel singleKeysNamePerDay1;
    private JLabel singleKeysNamePerDay2;
    private JLabel singleKeysNamePerDay3;
    private JLabel singleKeysNamePerDay4;
    private JLabel singleKeysNamePerDay5;
    private JLabel topKeysPerDayLabel;
    private JPanel dataPerLifePanel;
    private JSeparator langPerDaySeparator;
    private JLabel executionsPerDayLabel;
    private JLabel singleKeysValuePerDay1;
    private JLabel singleKeysValuePerDay2;
    private JLabel singleKeysValuePerDay3;
    private JLabel singleKeysValuePerDay4;
    private JLabel singleKeysValuePerDay5;
    private JLabel executionsPerLifeLabel;
    private JSeparator langPerLifeSeparator;
    private JLabel singleKeysNamePerLife1;
    private JLabel singleKeysNamePerLife2;
    private JLabel singleKeysNamePerLife3;
    private JLabel singleKeysNamePerLife4;
    private JLabel singleKeysNamePerLife5;
    private JLabel generalPerLifeLabel;
    private JPanel todayPanel;
    private JPanel overallPanel;
    private JLabel generalPerDayLabel;
    private JLabel successfulCommitsPerDayLabel;
    private JLabel failedCommitsPerDayLabel;
    private JLabel successfulExecutionsPerDayLabel;
    private JLabel failedExecutionsPerDayLabel;
    private JTabbedPane tabbedPanel;
    private JLabel failedCommitsPerLifeLabel;
    private JLabel successfulCommitsPerLifeLabel;
    private JLabel successfulExecutionsPerLifeLabel;
    private JLabel failedExecutionsPerLifeLabel;
    private ToolWindow toolWindow;
    private Balloon balloon;

    private PluginState state = PluginState.Companion.getInstance();

    public ToolWindowForm() {
    }

    @Override
    public void init(ToolWindow window) {
    }

    @Override
    public void createToolWindowContent(@NotNull Project project, @NotNull ToolWindow toolWindow) {
        this.toolWindow = toolWindow;
        addRootPanel();
        setToolWindowTitle();
        addListeners();
        setColors();
        setValues();
        setMargins();
        setIcons();
    }

    private void setToolWindowTitle() {
        if (rootPanel == null) {
            return;
        }
        Project project = ProjectUtils.Companion.getProject(rootPanel);
        String userName = state.getUserName();
        Team team = state.getTeamForCurrentProject(project);
        String teamName = team == null ? Constants.EMPTY_STRING : team.getName();
        String toolWindowTitle = Constants.EMPTY_STRING;

        if (!userName.equals(Constants.EMPTY_STRING)) {
            toolWindowTitle += userName;
        }
        if (!teamName.equals(Constants.EMPTY_STRING)) {
            toolWindowTitle += " [" + teamName + "]";
        }
        toolWindow.setTitle(toolWindowTitle);
    }

    private void addRootPanel() {
        ContentFactory contentFactory = ContentFactory.SERVICE.getInstance();

        //no need to display name it is already displayed
        toolWindow.getContentManager().addContent(contentFactory.createContent(rootPanel, /*name*/ Constants.EMPTY_STRING, false));
    }

    private void addListeners() {

        //-----------------------------------------------download-----------------------------------------------------------------//
        downloadPanel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                ProgressManager.getInstance().run(new Task.Backgroundable(ProjectUtils.Companion.getProject(rootPanel), Constants.DOWNLOADING_TEXT) {
                    @Override
                    public void run(@NotNull final ProgressIndicator progressIndicator) {
                        new SendStatisticsGetNotifications().run();
                        new GetUser().run();
                    }
                });

            }

            @Override
            public void mouseEntered(MouseEvent e) {
                downloadPanel.setBackground(Constants.Companion.getLABEL_BACKGROUND_COLOR_FOCUSED());
                downloadPanel.setBorder(BorderFactory.createLineBorder(Constants.Companion.getPANEL_BORDER_COLOR_FOCUSED()));
            }

            @Override
            public void mouseExited(MouseEvent e) {
                downloadPanel.setBackground(Constants.Companion.getBEIDGE_PANEL_BACKGROUND_COLOR());
                downloadPanel.setBorder(BorderFactory.createLineBorder(Constants.Companion.getBEIDGE_PANEL_BACKGROUND_COLOR()));
            }
        });

        //--------------------------------------------refresh----------------------------------------------------------//
        refreshPanel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                setToolWindowTitle();

                ProgressManager.getInstance().run(new Task.Backgroundable(ProjectUtils.Companion.getProject(rootPanel), Constants.REFRESHING_TEXT) {
                    @Override
                    public void run(@NotNull final ProgressIndicator progressIndicator) {
                        Team team = state.getTeamByProjectPath(ProjectUtils.Companion.getProjectId(rootPanel));
                        if (team != null) {
                            StateUtils.Companion.getInstance().refreshStateFromServer(team.getId());
                        }
                        setValues();
                    }
                });

            }

            @Override
            public void mouseEntered(MouseEvent e) {
                refreshPanel.setBackground(Constants.Companion.getLABEL_BACKGROUND_COLOR_FOCUSED());
                refreshPanel.setBorder(BorderFactory.createLineBorder(Constants.Companion.getPANEL_BORDER_COLOR_FOCUSED()));
            }

            @Override
            public void mouseExited(MouseEvent e) {
                refreshPanel.setBackground(Constants.Companion.getBEIDGE_PANEL_BACKGROUND_COLOR());
                refreshPanel.setBorder(BorderFactory.createLineBorder(Constants.Companion.getBEIDGE_PANEL_BACKGROUND_COLOR()));
            }
        });

        //TODO: set background color on focus
        //  rootPanel.setBackground(Constants.Companion.getPANEL_BACKGROUND_COLOR_FOCUSED());
    }

    private void setColors() {
        //---------------------------------------toolbar icons--------------------------------------------------------//
        downloadPanel.setBackground(Constants.Companion.getBEIDGE_PANEL_BACKGROUND_COLOR());
        refreshPanel.setBackground(Constants.Companion.getBEIDGE_PANEL_BACKGROUND_COLOR());
        downloadPanel.setBorder(BorderFactory.createLineBorder(Constants.Companion.getBEIDGE_PANEL_BACKGROUND_COLOR()));
        refreshPanel.setBorder(BorderFactory.createLineBorder(Constants.Companion.getBEIDGE_PANEL_BACKGROUND_COLOR()));

        //------------------------------------------panels------------------------------------------------------------//
        toolbar.setBackground(Constants.Companion.getBEIDGE_PANEL_BACKGROUND_COLOR());
        refreshPanel.setBackground(Constants.Companion.getBEIDGE_PANEL_BACKGROUND_COLOR());
        downloadPanel.setBackground(Constants.Companion.getBEIDGE_PANEL_BACKGROUND_COLOR());
        rootPanel.setBackground(Constants.Companion.getBEIDGE_PANEL_BACKGROUND_COLOR());

        //--------------------------------------invisible panels------------------------------------------------------//
        dataPerDayPanel.setBackground(Constants.Companion.getWHITE_PANEL_BACKGROUND_COLOR());
        dataPerLifePanel.setBackground(Constants.Companion.getWHITE_PANEL_BACKGROUND_COLOR());
        langPerDayPanel.setBackground(Constants.Companion.getWHITE_PANEL_BACKGROUND_COLOR());
        langPerLifePanel.setBackground(Constants.Companion.getWHITE_PANEL_BACKGROUND_COLOR());

        //--------------------------------------------labels----------------------------------------------------------//
        timePerDayValue.setForeground(Constants.Companion.getTEXT_COLOR());
        symbolsPerDayValue.setForeground(Constants.Companion.getTEXT_COLOR());
        symbolsPerLifeValue.setForeground(Constants.Companion.getTEXT_COLOR());
        timePerLifeValue.setForeground(Constants.Companion.getTEXT_COLOR());
        successfulCommitsPerLifeValue.setForeground(Constants.Companion.getTEXT_COLOR());
        successfulCommitsPerDayValue.setForeground(Constants.Companion.getTEXT_COLOR());
        singleKeysValuePerLife1.setForeground(Constants.Companion.getTEXT_COLOR());
        singleKeysNamePerDay1.setForeground(Constants.Companion.getTEXT_COLOR());
        maxContinuousTimePerDay.setForeground(Constants.Companion.getTEXT_COLOR());
        maxContinuousSymbolsPerDay.setForeground(Constants.Companion.getTEXT_COLOR());
        singleKeysValuePerLife2.setForeground(Constants.Companion.getTEXT_COLOR());
        singleKeysValuePerLife3.setForeground(Constants.Companion.getTEXT_COLOR());
        singleKeysValuePerLife5.setForeground(Constants.Companion.getTEXT_COLOR());
        singleKeysNamePerDay2.setForeground(Constants.Companion.getTEXT_COLOR());
        singleKeysNamePerDay3.setForeground(Constants.Companion.getTEXT_COLOR());
        singleKeysNamePerDay5.setForeground(Constants.Companion.getTEXT_COLOR());
        maxContinuousTimePerLife.setForeground(Constants.Companion.getTEXT_COLOR());
        maxContinuousSymbolsPerLife.setForeground(Constants.Companion.getTEXT_COLOR());
        failedCommitsPerLifeValue.setForeground(Constants.Companion.getTEXT_COLOR());
        failedCommitsPerDayValue.setForeground(Constants.Companion.getTEXT_COLOR());
        singleKeysValuePerLife4.setForeground(Constants.Companion.getTEXT_COLOR());
        singleKeysNamePerDay4.setForeground(Constants.Companion.getTEXT_COLOR());
        downloadLabel.setForeground(Constants.Companion.getTEXT_COLOR());
        refreshLabel.setForeground(Constants.Companion.getTEXT_COLOR());
        langTimePerDay1.setForeground(Constants.Companion.getTEXT_COLOR());
        langSymbolPerDay1.setForeground(Constants.Companion.getTEXT_COLOR());
        langTimePerDay2.setForeground(Constants.Companion.getTEXT_COLOR());
        langSymbolPerDay2.setForeground(Constants.Companion.getTEXT_COLOR());
        langPerDaySymbolsLabel.setForeground(Constants.Companion.getTEXT_COLOR());
        langPerDayName1.setForeground(Constants.Companion.getTEXT_COLOR());
        langPerDayName2.setForeground(Constants.Companion.getTEXT_COLOR());
        languagesPerDayLabel.setForeground(Constants.Companion.getTEXT_COLOR());
        langPerDayNameLabel.setForeground(Constants.Companion.getTEXT_COLOR());
        langPerDayTimeLabel.setForeground(Constants.Companion.getTEXT_COLOR());
        langPerDayName3.setForeground(Constants.Companion.getTEXT_COLOR());
        langTimePerDay3.setForeground(Constants.Companion.getTEXT_COLOR());
        langSymbolPerDay3.setForeground(Constants.Companion.getTEXT_COLOR());
        langPerDayName4.setForeground(Constants.Companion.getTEXT_COLOR());
        langPerDayName5.setForeground(Constants.Companion.getTEXT_COLOR());
        langTimePerDay4.setForeground(Constants.Companion.getTEXT_COLOR());
        langTimePerDay5.setForeground(Constants.Companion.getTEXT_COLOR());
        langSymbolPerDay4.setForeground(Constants.Companion.getTEXT_COLOR());
        langSymbolPerDay5.setForeground(Constants.Companion.getTEXT_COLOR());
        languagesPerLifeLabel.setForeground(Constants.Companion.getTEXT_COLOR());
        langPerLifeNameLabel.setForeground(Constants.Companion.getTEXT_COLOR());
        langPerLifeTimeLabel.setForeground(Constants.Companion.getTEXT_COLOR());
        langPerLifeName1.setForeground(Constants.Companion.getTEXT_COLOR());
        langPerLifeName2.setForeground(Constants.Companion.getTEXT_COLOR());
        langPerLifeName3.setForeground(Constants.Companion.getTEXT_COLOR());
        langPerLifeName4.setForeground(Constants.Companion.getTEXT_COLOR());
        langPerLifeName5.setForeground(Constants.Companion.getTEXT_COLOR());
        langTimePerLife1.setForeground(Constants.Companion.getTEXT_COLOR());
        langTimePerLife2.setForeground(Constants.Companion.getTEXT_COLOR());
        langTimePerLife3.setForeground(Constants.Companion.getTEXT_COLOR());
        langTimePerLife4.setForeground(Constants.Companion.getTEXT_COLOR());
        langTimePerLife5.setForeground(Constants.Companion.getTEXT_COLOR());
        langSymbolPerLife1.setForeground(Constants.Companion.getTEXT_COLOR());
        langSymbolPerLife2.setForeground(Constants.Companion.getTEXT_COLOR());
        langSymbolPerLife3.setForeground(Constants.Companion.getTEXT_COLOR());
        langSymbolPerLife4.setForeground(Constants.Companion.getTEXT_COLOR());
        langSymbolPerLife5.setForeground(Constants.Companion.getTEXT_COLOR());
        langPerLifeSymbolsLabel.setForeground(Constants.Companion.getTEXT_COLOR());
        timeTodayLabel.setForeground(Constants.Companion.getTEXT_COLOR());
        symbolsTodayLabel.setForeground(Constants.Companion.getTEXT_COLOR());
        timeOverallLabel.setForeground(Constants.Companion.getTEXT_COLOR());
        symbolsOverallLabel.setForeground(Constants.Companion.getTEXT_COLOR());
        commitsPerLifeLabel.setForeground(Constants.Companion.getTEXT_COLOR());
        commitsPerDayLabel.setForeground(Constants.Companion.getTEXT_COLOR());
        topKeysPerLifeLabel.setForeground(Constants.Companion.getTEXT_COLOR());
        topKeysPerDayLabel.setForeground(Constants.Companion.getTEXT_COLOR());
        continuousTodayLabel.setForeground(Constants.Companion.getTEXT_COLOR());
        maxTodayTimeLabel.setForeground(Constants.Companion.getTEXT_COLOR());
        maxTodaySymbolsLabel.setForeground(Constants.Companion.getTEXT_COLOR());
        continuousOverallLabel.setForeground(Constants.Companion.getTEXT_COLOR());
        maxOverallTimeLabel.setForeground(Constants.Companion.getTEXT_COLOR());
        maxOverallSymbolsLabel.setForeground(Constants.Companion.getTEXT_COLOR());
        failedExecutionsPerLifeValue.setForeground(Constants.Companion.getTEXT_COLOR());
        successfulExecutionsPerLifeValue.setForeground(Constants.Companion.getTEXT_COLOR());
        failedExecutionsPerDayValue.setForeground(Constants.Companion.getTEXT_COLOR());
        successfulExecutionsPerDayValue.setForeground(Constants.Companion.getTEXT_COLOR());
        //------------------------------------------------------------------------------------------------------------//
    }

    private void setMargins() {
        commitsPerDayLabel.setBorder(Constants.Companion.getSECTION_MARGIN());
        executionsPerDayLabel.setBorder(Constants.Companion.getSECTION_MARGIN());
        topKeysPerDayLabel.setBorder(Constants.Companion.getSECTION_MARGIN());
        languagesPerDayLabel.setBorder(Constants.Companion.getSECTION_MARGIN());
        continuousTodayLabel.setBorder(Constants.Companion.getSECTION_MARGIN());

        commitsPerLifeLabel.setBorder(Constants.Companion.getSECTION_MARGIN());
        executionsPerLifeLabel.setBorder(Constants.Companion.getSECTION_MARGIN());
        topKeysPerLifeLabel.setBorder(Constants.Companion.getSECTION_MARGIN());
        languagesPerLifeLabel.setBorder(Constants.Companion.getSECTION_MARGIN());
        continuousOverallLabel.setBorder(Constants.Companion.getSECTION_MARGIN());
    }

    private void setIcons() {
        downloadLabel.setIcon(IconLoader.getIcon(Constants.DOWNLOAD_ICON_PATH));
        refreshLabel.setIcon(IconLoader.getIcon(Constants.REFRESH_ICON_PATH));

        generalPerDayLabel.setIcon(IconLoader.getIcon(Constants.GENERAL_ICON_PATH));
        timeTodayLabel.setIcon(IconLoader.getIcon(Constants.TIME_ICON_PATH));
        symbolsTodayLabel.setIcon(IconLoader.getIcon(Constants.SYMBOLS_ICON_PATH));
        commitsPerDayLabel.setIcon(IconLoader.getIcon(Constants.COMMITS_ICON_PATH));
        failedCommitsPerDayLabel.setIcon(IconLoader.getIcon(Constants.FAILED_ICON_PATH));
        successfulCommitsPerDayLabel.setIcon(IconLoader.getIcon(Constants.SUCCESSFUL_ICON_PATH));
        executionsPerDayLabel.setIcon(IconLoader.getIcon(Constants.EXECUTIONS_ICON_PATH));
        failedExecutionsPerDayLabel.setIcon(IconLoader.getIcon(Constants.FAILED_ICON_PATH));
        successfulExecutionsPerDayLabel.setIcon(IconLoader.getIcon(Constants.SUCCESSFUL_ICON_PATH));
        topKeysPerDayLabel.setIcon(IconLoader.getIcon(Constants.KEYS_ICON_PATH));
        continuousTodayLabel.setIcon(IconLoader.getIcon(Constants.CONTINUOUS_ICON_PATH));
        maxTodayTimeLabel.setIcon(IconLoader.getIcon(Constants.TIME_ICON_PATH));
        maxTodaySymbolsLabel.setIcon(IconLoader.getIcon(Constants.SYMBOLS_ICON_PATH));

        generalPerLifeLabel.setIcon(IconLoader.getIcon(Constants.GENERAL_ICON_PATH));
        timeOverallLabel.setIcon(IconLoader.getIcon(Constants.TIME_ICON_PATH));
        symbolsOverallLabel.setIcon(IconLoader.getIcon(Constants.SYMBOLS_ICON_PATH));
        commitsPerLifeLabel.setIcon(IconLoader.getIcon(Constants.COMMITS_ICON_PATH));
        failedCommitsPerLifeLabel.setIcon(IconLoader.getIcon(Constants.FAILED_ICON_PATH));
        successfulCommitsPerLifeLabel.setIcon(IconLoader.getIcon(Constants.SUCCESSFUL_ICON_PATH));
        executionsPerLifeLabel.setIcon(IconLoader.getIcon(Constants.EXECUTIONS_ICON_PATH));
        failedExecutionsPerLifeLabel.setIcon(IconLoader.getIcon(Constants.FAILED_ICON_PATH));
        successfulExecutionsPerLifeLabel.setIcon(IconLoader.getIcon(Constants.SUCCESSFUL_ICON_PATH));
        topKeysPerLifeLabel.setIcon(IconLoader.getIcon(Constants.KEYS_ICON_PATH));
        continuousOverallLabel.setIcon(IconLoader.getIcon(Constants.CONTINUOUS_ICON_PATH));
        maxOverallTimeLabel.setIcon(IconLoader.getIcon(Constants.TIME_ICON_PATH));
        maxOverallSymbolsLabel.setIcon(IconLoader.getIcon(Constants.SYMBOLS_ICON_PATH));

        tabbedPanel.setIconAt(0, IconLoader.getIcon(Constants.TODAY_ICON_PATH));
        tabbedPanel.setIconAt(1, IconLoader.getIcon(Constants.SUM_ICON_PATH));
    }

    private void setValues() {
        Project project = ProjectUtils.Companion.getProject(rootPanel);
        Statistics statistics = state.getStatistics(project != null ? ProjectUtils.Companion.getProjectId(project) : Constants.DEFAULT_PROJECT_ID);
        if (statistics == null) {
            return;
        }

        //---------------------------------------------time, symbols-------------------------------------------------------//
        timePerDayValue.setText(TimeUtils.Companion.getInstance().millisToPrettyTime(statistics.getTimePerDay()));
        timePerLifeValue.setText(TimeUtils.Companion.getInstance().millisToPrettyTime(statistics.getTimePerLife()));

        Map<ContinuousType, Long> continuous = TimeUtils.Companion.getInstance().getContinuous(project);

        maxContinuousTimePerDay.setText(TimeUtils.Companion.getInstance().millisToPrettyTime(continuous.get(ContinuousType.TIME_PER_DAY)));
        maxContinuousTimePerLife.setText(TimeUtils.Companion.getInstance().millisToPrettyTime(continuous.get(ContinuousType.TIME_PER_LIFE)));
        maxContinuousSymbolsPerDay.setText(Long.toString(continuous.get(ContinuousType.SYMBOLS_PER_DAY)));
        maxContinuousSymbolsPerLife.setText(Long.toString(continuous.get(ContinuousType.SYMBOLS_PER_LIFE)));

        symbolsPerDayValue.setText(Long.toString(statistics.getSymbolsQuantityPerDay()));
        symbolsPerLifeValue.setText(Long.toString(statistics.getSymbolsQuantityPerLife()));

        //-------------------------------------------commits--------------------------------------------------------//
        successfulCommitsPerLifeValue.setText(Long.toString(statistics.getSuccessfulCommitsPerLife()));
        failedCommitsPerLifeValue.setText(Long.toString(statistics.getFailedCommitsPerLife()));

        successfulCommitsPerDayValue.setText(Long.toString(statistics.getSuccessfulCommitsPerDay()));
        failedCommitsPerDayValue.setText(Long.toString(statistics.getFailedCommitsPerDay()));

        //--------------------------------------------executions----------------------------------------------------//
        successfulExecutionsPerLifeValue.setText(Long.toString(statistics.getSuccessfulExecutionsPerLife()));
        failedExecutionsPerLifeValue.setText(Long.toString(statistics.getFailedExecutionsPerLife()));

        successfulExecutionsPerDayValue.setText(Long.toString(statistics.getSuccessfulExecutionsPerDay()));
        failedExecutionsPerDayValue.setText(Long.toString(statistics.getFailedExecutionsPerDay()));

        //--------------------------------------------keys----------------------------------------------------------//
        List<Map.Entry<String, KeyInfo>> keys = statistics.getKeyTapQuantity().entrySet()
                .stream()
                .sorted((o1, o2) -> Long.compare(o2.getValue().getPerLife(), o1.getValue().getPerLife()))
                .limit(10)
                .collect(Collectors.toList());

        if (keys.size() > 0) {
            singleKeysNamePerLife1.setIcon(IconLoader.getIcon(Constants.KEY_1_ICON_PATH));
            singleKeysNamePerLife1.setText(keys.get(0).getKey());
            singleKeysValuePerLife1.setText(Long.toString(keys.get(0).getValue().getPerLife()));

            if (keys.size() > 1) {
                singleKeysNamePerLife2.setIcon(IconLoader.getIcon(Constants.KEY_2_ICON_PATH));
                singleKeysNamePerLife2.setText(keys.get(1).getKey());
                singleKeysValuePerLife2.setText(Long.toString(keys.get(1).getValue().getPerLife()));

                if (keys.size() > 2) {
                    singleKeysNamePerLife3.setIcon(IconLoader.getIcon(Constants.KEY_3_ICON_PATH));
                    singleKeysNamePerLife3.setText(keys.get(2).getKey());
                    singleKeysValuePerLife3.setText(Long.toString(keys.get(2).getValue().getPerLife()));

                }

                if (keys.size() > 3) {
                    singleKeysNamePerLife4.setIcon(IconLoader.getIcon(Constants.KEY_4_ICON_PATH));
                    singleKeysNamePerLife4.setText(keys.get(3).getKey());
                    singleKeysValuePerLife4.setText(Long.toString(keys.get(3).getValue().getPerLife()));

                }

                if (keys.size() > 4) {
                    singleKeysNamePerLife5.setIcon(IconLoader.getIcon(Constants.KEY_5_ICON_PATH));
                    singleKeysNamePerLife5.setText(keys.get(4).getKey());
                    singleKeysValuePerLife5.setText(Long.toString(keys.get(4).getValue().getPerLife()));

                }
            }
        }


        keys.clear();
        keys = statistics.getKeyTapQuantity().entrySet()
                .stream()
                .sorted((o1, o2) -> Long.compare(o2.getValue().getPerDay(), o1.getValue().getPerDay()))
                .limit(10)
                .collect(Collectors.toList());
        if (keys.size() > 0) {
            singleKeysNamePerDay1.setIcon(IconLoader.getIcon(Constants.KEY_1_ICON_PATH));
            singleKeysNamePerDay1.setText(keys.get(0).getKey());
            singleKeysValuePerDay1.setText(Long.toString(keys.get(0).getValue().getPerDay()));

            if (keys.size() > 1) {
                singleKeysNamePerDay2.setIcon(IconLoader.getIcon(Constants.KEY_2_ICON_PATH));
                singleKeysNamePerDay2.setText(keys.get(1).getKey());
                singleKeysValuePerDay2.setText(Long.toString(keys.get(1).getValue().getPerDay()));

                if (keys.size() > 2) {
                    singleKeysNamePerDay3.setIcon(IconLoader.getIcon(Constants.KEY_3_ICON_PATH));
                    singleKeysNamePerDay3.setText(keys.get(2).getKey());
                    singleKeysValuePerDay3.setText(Long.toString(keys.get(2).getValue().getPerDay()));

                }

                if (keys.size() > 3) {
                    singleKeysNamePerDay4.setIcon(IconLoader.getIcon(Constants.KEY_4_ICON_PATH));
                    singleKeysNamePerDay4.setText(keys.get(3).getKey());
                    singleKeysValuePerDay4.setText(Long.toString(keys.get(3).getValue().getPerDay()));

                }

                if (keys.size() > 4) {
                    singleKeysNamePerDay5.setIcon(IconLoader.getIcon(Constants.KEY_5_ICON_PATH));
                    singleKeysNamePerDay5.setText(keys.get(4).getKey());
                    singleKeysValuePerDay5.setText(Long.toString(keys.get(4).getValue().getPerDay()));

                }
            }
        }

        //-----------------------------------------languages------------------------------------------------------------//
        List<Map.Entry<String, LanguageInfo>> languages = statistics.getLanguageInfo().entrySet()
                .stream()
                .filter(entry -> !(entry.getValue().getSymbolsPerDay() == 0))
                .sorted(Comparator.comparing(Map.Entry::getValue))
                .limit(5)
                .collect(Collectors.toList());

        if (languages.isEmpty()) {
            languagesPerDayLabel.setText(Constants.EMPTY_STRING);
            languagesPerDayLabel.setIcon(null);
            langPerDaySeparator.setVisible(false);
            langPerDayPanel.setVisible(false);

        } else {
            langPerDayPanel.setVisible(true);
            langPerDaySeparator.setVisible(true);
            languagesPerDayLabel.setText(Constants.LANGUAGES);
            languagesPerDayLabel.setIcon(IconLoader.getIcon(Constants.LANGUAGES_ICON_PATH));
            langPerDayNameLabel.setIcon(IconLoader.getIcon(Constants.LANGUAGES_ICON_PATH));
            langPerDayTimeLabel.setIcon(IconLoader.getIcon(Constants.TIME_ICON_PATH));
            langPerDaySymbolsLabel.setIcon(IconLoader.getIcon(Constants.SYMBOLS_ICON_PATH));

            langPerDayName1.setText(languages.get(0).getKey());
            langSymbolPerDay1.setText(Long.toString(languages.get(0).getValue().getSymbolsPerDay()));
            langTimePerDay1.setText(TimeUtils.Companion.getInstance().millisToPrettyTime(languages.get(0).getValue().getTimePerDay()));

            if (languages.size() > 1) {
                langPerDayName2.setText(languages.get(1).getKey());
                langSymbolPerDay2.setText(Long.toString(languages.get(1).getValue().getSymbolsPerDay()));
                langTimePerDay2.setText(TimeUtils.Companion.getInstance().millisToPrettyTime(languages.get(1).getValue().getTimePerDay()));

                if (languages.size() > 2) {
                    langPerDayName3.setText(languages.get(2).getKey());
                    langSymbolPerDay3.setText(Long.toString(languages.get(2).getValue().getSymbolsPerDay()));
                    langTimePerDay3.setText(TimeUtils.Companion.getInstance().millisToPrettyTime(languages.get(2).getValue().getTimePerDay()));

                    if (languages.size() > 3) {
                        langPerDayName4.setText(languages.get(3).getKey());
                        langSymbolPerDay4.setText(Long.toString(languages.get(3).getValue().getSymbolsPerDay()));
                        langTimePerDay4.setText(TimeUtils.Companion.getInstance().millisToPrettyTime(languages.get(3).getValue().getTimePerDay()));
                    }

                    if (languages.size() > 4) {
                        langPerDayName5.setText(languages.get(4).getKey());
                        langSymbolPerDay5.setText(Long.toString(languages.get(4).getValue().getSymbolsPerDay()));
                        langTimePerDay5.setText(TimeUtils.Companion.getInstance().millisToPrettyTime(languages.get(4).getValue().getTimePerDay()));
                    }

                }
            }
        }

        languages.clear();
        languages = statistics.getLanguageInfo().entrySet()
                .stream()
                .filter(entry -> !(entry.getValue().getSymbolsPerLife() == 0))
                .sorted(Comparator.comparing(Map.Entry::getValue))
                .limit(5)
                .collect(Collectors.toList());

        if (languages.isEmpty()) {
            languagesPerLifeLabel.setText(Constants.EMPTY_STRING);
            languagesPerLifeLabel.setIcon(null);
            langPerLifeSeparator.setVisible(false);
            langPerLifePanel.setVisible(false);

        } else {
            langPerLifePanel.setVisible(true);
            langPerLifeSeparator.setVisible(true);
            languagesPerLifeLabel.setText(Constants.LANGUAGES);
            languagesPerLifeLabel.setIcon(IconLoader.getIcon(Constants.LANGUAGES_ICON_PATH));
            langPerLifeNameLabel.setIcon(IconLoader.getIcon(Constants.LANGUAGES_ICON_PATH));
            langPerLifeTimeLabel.setIcon(IconLoader.getIcon(Constants.TIME_ICON_PATH));
            langPerLifeSymbolsLabel.setIcon(IconLoader.getIcon(Constants.SYMBOLS_ICON_PATH));

            langPerLifeName1.setText(languages.get(0).getKey());
            langSymbolPerLife1.setText(Long.toString(languages.get(0).getValue().getSymbolsPerLife()));
            langTimePerLife1.setText(TimeUtils.Companion.getInstance().millisToPrettyTime(languages.get(0).getValue().getTimePerLife()));

            if (languages.size() > 1) {
                langPerLifeName2.setText(languages.get(1).getKey());
                langSymbolPerLife2.setText(Long.toString(languages.get(1).getValue().getSymbolsPerLife()));
                langTimePerLife2.setText(TimeUtils.Companion.getInstance().millisToPrettyTime(languages.get(1).getValue().getTimePerLife()));

                if (languages.size() > 2) {
                    langPerLifeName3.setText(languages.get(2).getKey());
                    langSymbolPerLife3.setText(Long.toString(languages.get(2).getValue().getSymbolsPerLife()));
                    langTimePerLife3.setText(TimeUtils.Companion.getInstance().millisToPrettyTime(languages.get(2).getValue().getTimePerLife()));

                    if (languages.size() > 3) {
                        langPerLifeName4.setText(languages.get(3).getKey());
                        langSymbolPerLife4.setText(Long.toString(languages.get(3).getValue().getSymbolsPerLife()));
                        langTimePerLife4.setText(TimeUtils.Companion.getInstance().millisToPrettyTime(languages.get(3).getValue().getTimePerLife()));
                    }

                    if (languages.size() > 4) {
                        langPerLifeName5.setText(languages.get(4).getKey());
                        langSymbolPerLife5.setText(Long.toString(languages.get(4).getValue().getSymbolsPerLife()));
                        langTimePerLife5.setText(TimeUtils.Companion.getInstance().millisToPrettyTime(languages.get(4).getValue().getTimePerDay()));
                    }
                }
            }
        }
        //------------------------------------------------------------------------------------------------------------------------------------------------//
    }
}
