package ru.vsu.noidle.model

import ru.vsu.noidle.analyser.KeysAnalyser
import ru.vsu.noidle.state.PluginState
import ru.vsu.noidle.ui.tool_window.ToolWindowForm
import ru.vsu.noidle.util.Constants

/**
 * Used to keep statistics by languages
 *
 * @see [PluginState.languageInfo]
 * @see [KeysAnalyser.updateLanguageInfo]
 * @see [ToolWindowForm.setValues]
 */
data class LanguageInfo(
        var name: Language = Language.OTHER,
        var timePerDay: Long = Constants.DEFAULT_LONG,
        var timePerLife: Long = Constants.DEFAULT_LONG,
        var symbolsPerDay: Long = Constants.DEFAULT_LONG,
        var symbolsPerLife: Long = Constants.DEFAULT_LONG

) : Comparable<LanguageInfo> {

    override fun compareTo(other: LanguageInfo): Int {
        return if (this.symbolsPerDay == other.symbolsPerDay) 0 else if (this.symbolsPerDay > other.symbolsPerDay) -1 else 1
    }
}