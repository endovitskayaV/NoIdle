package ru.vsu.noidle.model

import ru.vsu.noidle.util.Constants

data class KeyInfo(
        var perDay: Long = Constants.DEFAULT_LONG,
        var perLife: Long = Constants.DEFAULT_LONG
)