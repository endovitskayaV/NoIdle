package ru.vsu.noidle.model.dto

import ru.vsu.noidle.util.Constants
import java.time.OffsetDateTime
import java.util.*

data class StatisticsDto(
        var id: UUID = UUID(Constants.DEFAULT_LONG, Constants.DEFAULT_LONG),
        var type: StatisticsType = StatisticsType.TIME,
        var subType: StatisticsSubType = StatisticsSubType.PER_DAY,
        var extraValue: String? = null,
        var value: Long = Constants.DEFAULT_LONG,
        var date: String = OffsetDateTime.now().toString()
)