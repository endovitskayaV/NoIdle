package ru.vsu.noidle.model.dto

import ru.vsu.noidle.util.Constants
import java.util.*

data class UserDto(
        var id: UUID = UUID(Constants.DEFAULT_LONG, Constants.DEFAULT_LONG),
        var email: String = Constants.EMPTY_STRING,
        var name: String = Constants.EMPTY_STRING,
        var photo: String? = null,
        var requirements: Collection<RequirementDto>? = null
)