package ru.vsu.noidle.model.dto

data class NotificationDto(
        var achievement: AchievementDto,
        var user: UserDtoForNotification,
        var requirements: List<RequirementDto>,
        val date: Long,
        val team: TeamDto?
)