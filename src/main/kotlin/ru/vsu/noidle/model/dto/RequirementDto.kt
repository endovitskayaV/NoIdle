package ru.vsu.noidle.model.dto

data class RequirementDto(
        var statisticsType: StatisticsType,
        var statisticsSubType: StatisticsSubType,
        var extraValue: String? = null,
        var value: Long,
        var teamContributionRate: Float? = null
)
