package ru.vsu.noidle.model.dto

/**
 * Statistics info type
 */
enum class StatisticsType(val shortcut: String) {
    TIME("time"),
    SYMBOL("symbol"),
    COMMIT("commit"),
    EXEC("exec"),
    LANG_TIME("lang_time"),
    LANG_SYMBOL("lang_symbol"),
    SINGLE_KEY("single_key");

    companion object {

        fun byShortcut(name: String): StatisticsType? {
            for (statisticsType in StatisticsType.values()) {
                if (statisticsType.shortcut == name) {
                    return statisticsType
                }
            }
            return null
        }
    }
}