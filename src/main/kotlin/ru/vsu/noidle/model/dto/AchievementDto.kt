package ru.vsu.noidle.model.dto

data class AchievementDto(
        var levelNumber: Long?,
        var name: String,
        var type: AchievementType
)