package ru.vsu.noidle.model.dto

/**
 * Statistics info subtype
 */
enum class StatisticsSubType(val shortcut: String) {
    PER_LIFE            ("per_life"),
    PER_DAY             ("per_day"),
    CONTINUOUS_PER_LIFE ("continuous_per_life"),
    CONTINUOUS_PER_DAY  ("continuous_per_day");

    companion object {

        fun byShortcut(name: String): StatisticsSubType? {
            for (subType in StatisticsSubType.values()) {
                if (subType.shortcut == name) {
                    return subType
                }
            }
            return null
        }
    }
}