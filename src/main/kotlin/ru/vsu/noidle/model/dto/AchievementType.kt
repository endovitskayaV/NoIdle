package ru.vsu.noidle.model.dto

enum class AchievementType (val shortcut: String){

    LEVEL("level"),
    EXTRA("extra"),
    TEAM("team");

    companion object {

        fun byShortcut(name: String): AchievementType? {
            for (achievementType in AchievementType.values()) {
                if (achievementType.shortcut == name) {
                    return achievementType
                }
            }
            return null
        }
    }
}