package ru.vsu.noidle.model.dto

import ru.vsu.noidle.util.Constants
import java.util.*

data class TeamDto(
        var id: UUID=Constants.ZERO_UUID,
        var name: String= Constants.EMPTY_STRING
)