package ru.vsu.noidle.model.dto

data class UserDtoForNotification(var email:String, var name: String)