package ru.vsu.noidle.model

import ru.vsu.noidle.util.Constants.Companion.DEFAULT_TEAM_ID
import ru.vsu.noidle.util.Constants.Companion.DEFAULT_TEAM_NAME

data class Team(
        var id: String=DEFAULT_TEAM_ID,
        var name: String= DEFAULT_TEAM_NAME
)