package ru.vsu.noidle.model

import ru.vsu.noidle.util.TimeUtils

/**
 * Continuous data names
 *
 * @see [TimeUtils.getContinuous]
 */
enum class ContinuousType {
    TIME_PER_DAY,
    TIME_PER_LIFE,
    SYMBOLS_PER_DAY,
    SYMBOLS_PER_LIFE
}