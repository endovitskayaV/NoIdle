package ru.vsu.noidle.model

import ru.vsu.noidle.sender.SendStatisticsGetNotifications
import ru.vsu.noidle.util.TimeUtils

/**
 * Continuous data
 *
 * @see [TimeUtils.getContinuous]
 * @see [SendStatisticsGetNotifications.needToSend]
 */
data class Continuous (
        var type:ContinuousType,
        var value:Long
)