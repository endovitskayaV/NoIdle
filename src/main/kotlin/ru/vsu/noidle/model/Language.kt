package ru.vsu.noidle.model

import ru.vsu.noidle.analyser.KeysAnalyser
import ru.vsu.noidle.state.PluginState

/**
 * @see [LanguageInfo]
 * @see [PluginState.languageInfo]
 * @see [KeysAnalyser.updateLanguageInfo]
 */

enum class Language(var shortcut: String, var extensions: Set<String>) {

    ACTION_SCRIPT   ("ActionScript",   setOf("as")),
    ASSEMBLY        ("Assembly",       setOf("asm", "s", "a")),
    AWK             ("Awk",            setOf("awk")),
    BATCH           ("Batch",          setOf("bat", "cmd", "btm")),
    C               ("C",              setOf("c")),
    C_SHARP         ("C#",             setOf("cs")),
    CPP             ("C++",            setOf("cpp", "cc", "cxx", "c++", "hh", "hpp")),
    CLOJURE         ("Clojure",        setOf("clj", "edn")),
    COFFEE_SCRIPT   ("CoffeeScript",   setOf("coffee", "litcoffee")),
    CSS             ("Css",            setOf("css")),
    CSV             ("CSV",            setOf("csv")),
    FREE_MARKER     ("FreeMarker",     setOf("ftl")),
    GO              ("Go",             setOf("go")),
    GRADLE          ("Gradle",         setOf("gradle")),
    GROOVY          ("Groovy",         setOf("groovy")),
    HASKEL          ("Haskel",         setOf("hs", "lhs")),
    HTML            ("Html",           setOf("html", "htm", "xhtml", "jhtml")),
    JAVA            ("Java",           setOf("java", "class", "jar", "jsp", "jspx")),
    JS              ("JavaScript",     setOf("js")),
    JSON            ("Json",           setOf("json")),
    KOTLIN          ("Kotlin",         setOf("kt")),
    LIVE_SCRIPT     ("LiveScript",     setOf("ls")),
    MARKDOWN        ("Markdown",       setOf("md")),
    MATLAB          ("Matlab",         setOf("m", "mlx")),
    PERL            ("Perl",           setOf("pl", "pm", "ph")),
    PHP             ("Php",            setOf("php")),
    POWER_SHELL     ("PowerShell",     setOf("ps1", "psm1")),
    PROPERTIES      ("Properties",     setOf("properties")),
    PYTHON          ("Python",         setOf("py", "pyc", "pyo", "pyd", "whl")),
    R               ("R",              setOf("R")),
    RUBY            ("Ruby",           setOf("rb", "rbw")),
    RUST            ("Rust",           setOf("rs")),
    SCALA           ("Scala",          setOf("scala")),
    SQL             ("Sql",            setOf("sql")),
    SWIFT           ("Swift",          setOf("swift")),
    SHELL           ("Shell",          setOf("sh")),
    TYPESCRIPT      ("TypeScript",     setOf("ts")),
    XML             ("Xml",            setOf("xml", "xsl", "xslt")),

    OTHER           ("other",          emptySet());

    companion object {

        fun byExtension(extension: String?): Language {
            if (extension == null) {
                return OTHER
            }
            for (language in Language.values()) {
                if (language.extensions.contains(extension)) {
                    return language
                }
            }
            return OTHER
        }
    }
}