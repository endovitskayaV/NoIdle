package ru.vsu.noidle.util

import com.intellij.openapi.project.Project
import ru.vsu.noidle.analyser.KeysAnalyser
import ru.vsu.noidle.model.ContinuousType
import ru.vsu.noidle.state.PluginState
import ru.vsu.noidle.state.Statistics
import ru.vsu.noidle.util.Constants.Companion.D
import ru.vsu.noidle.util.Constants.Companion.EMPTY_STRING
import ru.vsu.noidle.util.Constants.Companion.H
import ru.vsu.noidle.util.Constants.Companion.LESS_THAN_MIN
import ru.vsu.noidle.util.Constants.Companion.MIN
import ru.vsu.noidle.util.Constants.Companion.MON
import ru.vsu.noidle.util.Constants.Companion.Y
import java.util.*

class TimeUtils {
    companion object {

        @Volatile
        private var INSTANCE: TimeUtils? = null

        fun getInstance(): TimeUtils =
                INSTANCE ?: synchronized(this) {
                    INSTANCE ?: TimeUtils().also { INSTANCE = it }
                }
    }

    private var keysAnalyser = KeysAnalyser.getInstance()
    private var state = PluginState.getInstance()

    private var currentContinuousTypingTime: Long = 0L
    private var timeBetweenTaps = 0L

    private val SECOND = 1000
    private val MINUTE = 60 * SECOND
    private val HOUR = 60 * MINUTE
    private val DAY = 24L * HOUR
    private val MONTH = 31L * DAY
    private val YEAR = 12L * MONTH


    /**
     * Calculates continuous time and symbols before continuous typing stopped
     * @return continuous time and symbols
     */
    fun getContinuous(statistics: Statistics?): Map<ContinuousType, Long> {
        val continuous = HashMap<ContinuousType, Long>() //name-> value
        if (statistics == null) {
            return continuous
        }

        this.currentContinuousTypingTime = keysAnalyser.currentContinuousTypingTime

        val now = System.currentTimeMillis()

        //if no type was made then  timeBetweenTaps=0 else  timeBetweenTaps= now - lastTapTime
        this.timeBetweenTaps = now - (if (keysAnalyser.lastTapTime == 0L) now else keysAnalyser.lastTapTime)
        if (this.timeBetweenTaps <= Constants.ALLOWED_IDLE_MILLIS) {
            this.currentContinuousTypingTime += this.timeBetweenTaps
        }

        //time
        continuous[ContinuousType.TIME_PER_DAY] =
                if (this.currentContinuousTypingTime > statistics.maxContinuousTimePerDay)
                    this.currentContinuousTypingTime
                else
                    statistics.maxContinuousTimePerDay

        continuous[ContinuousType.TIME_PER_LIFE] =
                if (this.currentContinuousTypingTime > statistics.maxContinuousTimePerLife)
                    this.currentContinuousTypingTime
                else
                    statistics.maxContinuousTimePerLife


        //symbols
        continuous[ContinuousType.SYMBOLS_PER_DAY] =
                if (keysAnalyser.currentContinuousTypedSymbols > statistics.maxContinuousSymbolsPerDay)
                    keysAnalyser.currentContinuousTypedSymbols
                else
                    statistics.maxContinuousSymbolsPerDay

        continuous[ContinuousType.SYMBOLS_PER_LIFE] =
                if (keysAnalyser.currentContinuousTypedSymbols > statistics.maxContinuousSymbolsPerLife)
                    keysAnalyser.currentContinuousTypedSymbols
                else
                    statistics.maxContinuousSymbolsPerLife

        return continuous
    }

    fun getContinuous(project: Project?): Map<ContinuousType, Long> {
        val statistics = state.getStatistics(ProjectUtils.getProjectId(project) ?: Constants.DEFAULT_PROJECT_ID)
        return getContinuous(statistics)
    }

    fun changedContinuous(statistics: Statistics?): Boolean {
        val continuous = HashMap<ContinuousType, Long>() //name-> value
        if (statistics == null) {
            return false
        }

        this.currentContinuousTypingTime = keysAnalyser.currentContinuousTypingTime

        val now = System.currentTimeMillis()

        //if no type was made then  timeBetweenTaps=0 else  timeBetweenTaps= now - lastTapTime
        this.timeBetweenTaps = now - (if (keysAnalyser.lastTapTime == 0L) now else keysAnalyser.lastTapTime)
        if (this.timeBetweenTaps <= Constants.ALLOWED_IDLE_MILLIS) {
            this.currentContinuousTypingTime += this.timeBetweenTaps
        }

        //time
        if (this.currentContinuousTypingTime > statistics.maxContinuousTimePerDay) {
            continuous[ContinuousType.TIME_PER_DAY] = this.currentContinuousTypingTime
        }

        if (this.currentContinuousTypingTime > statistics.maxContinuousTimePerLife) {
            continuous[ContinuousType.TIME_PER_LIFE] = this.currentContinuousTypingTime
        }

        //symbols
        if (keysAnalyser.currentContinuousTypedSymbols > statistics.maxContinuousSymbolsPerDay) {
            continuous[ContinuousType.SYMBOLS_PER_DAY] = keysAnalyser.currentContinuousTypedSymbols
        }

        if (keysAnalyser.currentContinuousTypedSymbols > statistics.maxContinuousSymbolsPerLife) {
            continuous[ContinuousType.SYMBOLS_PER_LIFE] = keysAnalyser.currentContinuousTypedSymbols
        }

        return continuous.isEmpty()
    }

    fun millisToPrettyTime(millis: Long): String {
        val text = StringBuffer(EMPTY_STRING)
        var ms = millis

        if (ms >= YEAR) {
            text.append(ms / YEAR).append(" $Y ")
            ms %= YEAR
        }

        if (ms >= MONTH) {
            text.append(ms / MONTH).append(" $MON ")
            ms %= MONTH
        }

        if (ms >= DAY) {
            text.append(ms / DAY).append(" $D ")
            ms %= DAY
        }
        if (ms > HOUR) {
            text.append(ms / HOUR).append(" $H ")
            ms %= HOUR
        }
        if (ms > MINUTE) {
            text.append(ms / MINUTE).append(" $MIN ")
            ms %= MINUTE
        }
//        if (ms > SECOND) {
//            text.append(ms / SECOND).append(" sec ")
//            ms %= SECOND
//        }
        if (text.isEmpty()) {
            text.append(LESS_THAN_MIN)
        }
        return text.toString()
    }
}