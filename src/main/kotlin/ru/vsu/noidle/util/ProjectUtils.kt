package ru.vsu.noidle.util

import com.intellij.ide.DataManager
import com.intellij.openapi.actionSystem.PlatformDataKeys
import com.intellij.openapi.project.Project
import java.awt.Component

class ProjectUtils {
    companion object {
        fun getProject(component: Component) = PlatformDataKeys.PROJECT.getData(DataManager.getInstance().getDataContext(component))

        fun getProjectId(project: Project?) = project?.presentableUrl

        fun getProjectId(component: Component) = getProjectId(getProject(component))
    }
}