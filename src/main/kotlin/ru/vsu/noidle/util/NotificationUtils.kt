package ru.vsu.noidle.util

import com.intellij.notification.Notification
import com.intellij.notification.NotificationType
import com.intellij.notification.Notifications
import com.intellij.openapi.project.Project
import com.intellij.openapi.util.IconLoader
import ru.vsu.noidle.model.dto.*
import ru.vsu.noidle.state.PluginState
import ru.vsu.noidle.util.Constants.Companion.ACHIEVEMENT
import ru.vsu.noidle.util.Constants.Companion.DATE_FORMAT
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import javax.swing.Icon

/**
 * Forms notifications from server to sho notifications to user
 */
class NotificationUtils {
    companion object {
        @Volatile
        private var INSTANCE: NotificationUtils? = null

        fun getInstance(): NotificationUtils =
                INSTANCE ?: synchronized(this) {
                    INSTANCE ?: NotificationUtils().also { INSTANCE = it }
                }
    }

    private val state = PluginState.getInstance()

    fun showNotification(icon: Icon?, title: String?, subTitle: String?, content: String?, project: Project? = null) {
        Notifications.Bus.notify(Notification(
                Constants.PLUGIN_NAME,
                icon,
                title,
                subTitle,
                content,
                NotificationType.INFORMATION,
                null /*listener*/
        ), project)
    }

    fun showNotifications(notifications: Array<NotificationDto>) {
        notifications.sortBy { notificationDto -> notificationDto.date }
        val showDate = notifications.size > 5

        notifications.forEach {

            var title: String? = null
            var subTitle: String? = null
            val levelAchieved = "${level(it.achievement.levelNumber)}${it.achievement.name} ${Constants.ACHIEVED}${team(it.team)}" +
                    "${if (showDate) date(it.date) else ""}${Constants.COLON}"

            if (it.user.email.equals(state.email)) { //notification about yourself
                title = levelAchieved

            } else { //notification about colleague
                title = "${it.user.name} $ACHIEVEMENT"
                subTitle = levelAchieved
            }

            showNotification(
                    IconLoader.getIcon(Constants.NOTIFICATION_ICON_PATH),
                    title,
                    subTitle,
                    formMessage(it.requirements)
            )
        }
    }

    private fun date(date: Long) = " ${SimpleDateFormat(DATE_FORMAT).format(Date(date))}"

    private fun team(team: TeamDto?) = if (team != null) " (${team.name})" else Constants.EMPTY_STRING

    private fun level(levelNumber: Long?) = if (levelNumber != null) "${Constants.LEVEL} $levelNumber " else Constants.EMPTY_STRING

    private fun formMessage(requirements: List<RequirementDto>): String {
        var message = Constants.EMPTY_STRING
        requirements.forEach {
            message += when (it.statisticsType) {
                StatisticsType.TIME ->
                    "${Constants.DASH}${period(it.statisticsSubType)}${continuously(it.statisticsSubType)}${Constants.CODING_TIME}${Constants.COLON} " +
                            TimeUtils.getInstance().millisToPrettyTime(it.value)
                StatisticsType.SYMBOL, StatisticsType.SINGLE_KEY ->
                    if (it.extraValue == null)
                        "${Constants.DASH}${period(it.statisticsSubType)}${continuously(it.statisticsSubType)}${Constants.TYPED} ${Constants.SYMBOLS}${Constants.COLON} ${it.value}"
                    else named(it.extraValue, it.value)

                StatisticsType.COMMIT -> "${Constants.DASH}${it.extraValue} ${Constants.COMMITS}${Constants.COLON} ${it.value}"

                StatisticsType.EXEC -> "${Constants.DASH}${it.extraValue} ${Constants.EXECUTIONS}${Constants.COLON} ${it.value}"

                StatisticsType.LANG_SYMBOL -> "-${period(it.statisticsSubType)} ${Constants.TYPED} ${Constants.SYMBOLS} ${Constants.ON} ${it.extraValue}${Constants.COLON} ${it.value}"

                StatisticsType.LANG_TIME ->
                    "${Constants.DASH}${period(it.statisticsSubType)} ${it.extraValue} ${Constants.CODING_TIME}${Constants.COLON} " +
                            TimeUtils.getInstance().millisToPrettyTime(it.value)
            }
            message += "\n"
        }
        return message
    }

    private fun continuously(sunTypeStatistics: StatisticsSubType) =
            if (sunTypeStatistics == StatisticsSubType.CONTINUOUS_PER_LIFE || sunTypeStatistics == StatisticsSubType.CONTINUOUS_PER_DAY)
                "${Constants.CONTINUOUSLY} "
            else Constants.EMPTY_STRING

    private fun period(statisticsSubType: StatisticsSubType) =
            when (statisticsSubType) {
                StatisticsSubType.CONTINUOUS_PER_LIFE, StatisticsSubType.PER_LIFE -> "${Constants.OVERALL} "
                StatisticsSubType.CONTINUOUS_PER_DAY, StatisticsSubType.PER_DAY -> "${Constants.TODAY} "
                else -> Constants.EMPTY_STRING
            }

    private fun named(name: String?, value: Long) = "${Constants.DASH}${Constants.TYPED} ${Constants.KEY} '$name'${Constants.COLON} $value"

}