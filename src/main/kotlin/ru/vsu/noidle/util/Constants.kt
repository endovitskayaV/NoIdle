package ru.vsu.noidle.util

import com.intellij.ui.Gray
import com.intellij.ui.JBColor
import ru.vsu.noidle.model.Team
import java.awt.Color
import java.util.*
import javax.swing.BorderFactory

class Constants {
    companion object {
        const val PLUGIN_NAME = "NoIdle"

        //------------state------------//
        const val XML_NAME = "noidle.xml"
        const val STATE_COMPONENT_NAME = "PluginState"

        //-------------------http------------------//
        const val DEFAULT_SCHEME = "https"
        const val DEFAULT_HOST = "noidle.herokuapp.com"
//        const val DEFAULT_SCHEME = "http"
//        const val DEFAULT_HOST = "localhost:8081"

        //const val URI = "$DEFAULT_SCHEME://$DEFAULT_HOST"
        const val SAVE = "/save"
        const val STATISTICS = "/statistics"
        const val CHECK = "/check"
        const val NOTIFICATIONS = "/notifications"
        const val USERS = "/users/"
        const val TEAMS = "/teams"
        const val SHORT = "/short"
        const val USER_ID_PARAM = "userId"
        const val TEAM_ID_PARAM = "teamId"
        const val TEAM_NAME_PARAM = "name"
        const val TYPES_PARAM = "types"
        const val ADD = "/add/"

        const val OK = 200
        const val NO_CONTENT = 204

        const val SEND_TO_SERVER_DELAY = 10_000L
        const val SEND_TO_SERVER_PERIOD = 10_000L

        const val GET_FROM_SERVER_MIN_DELAY = 30_000L
        const val GET_FROM_SERVER_MIN_PERIOD = 30_000L

        const val GET_FROM_SERVER_DELAY = 60_000L
        const val GET_FROM_SERVER_PERIOD = 60_000L

        const val GET_FROM_SERVER_LONG_DELAY = 60 * 60_000L //1h
        const val GET_FROM_SERVER_LONG_PERIOD = 12 * 60 * 60_000L //12h

        const val REMOVE_DELAY = 31 * 24 * 60 * 60_000L //1mon
        const val REMOVE_PERIOD = 31 * 24 * 60 * 60_000L //1mon

        const val SPACE = ' '
        const val SPACE_REPLACEMENT = '_'

        //------------icons--------------------//
        const val NOTIFICATION_ICON_PATH = "/icons/notification.png"
        const val LANGUAGES_ICON_PATH = "/icons/languages.png"
        const val TIME_ICON_PATH = "/icons/time.png"
        const val SYMBOLS_ICON_PATH = "/icons/symbols.png"
        const val KEY_1_ICON_PATH = "/icons/key1.png"
        const val KEY_2_ICON_PATH = "/icons/key2.png"
        const val KEY_3_ICON_PATH = "/icons/key3.png"
        const val KEY_4_ICON_PATH = "/icons/key4.png"
        const val KEY_5_ICON_PATH = "/icons/key5.png"
        const val FAILED_ICON_PATH = "/icons/failed.png"
        const val COMMITS_ICON_PATH = "/icons/commits.png"
        const val EXECUTIONS_ICON_PATH = "/icons/executions.png"
        const val SUCCESSFUL_ICON_PATH = "/icons/successful.png"
        const val GENERAL_ICON_PATH = "/icons/general.png"
        const val KEYS_ICON_PATH = "/icons/keys.png"
        const val SUM_ICON_PATH = "/icons/sum.png"
        const val TODAY_ICON_PATH = "/icons/today.png"
        const val CONTINUOUS_ICON_PATH = "/icons/continuous.png"
        const val REFRESH_ICON_PATH = "/icons/refresh.png"
        const val DOWNLOAD_ICON_PATH = "/icons/download.png"
        //-------------------------------------//

        //--------------notifications---------//
        const val SUCCESSFUL = "successful"
        const val FAILED = "failed"
        const val ACHIEVEMENT_NOTIFICATION_TITLE = "Achievement"
        const val ACHIEVEMENT_NOTIFICATION_SUB_TITLE = "hard-working"
        const val LEVEL = "Level"
        const val HOUR = "hour"
        const val HOURS = "${HOUR}s"
        const val Y = 'y'
        const val MON = "mon"
        const val D = 'd'
        const val H = "h"
        const val MIN = "min"
        const val LANGUAGES = "languages"
        const val TIME = "time"
        const val SYMBOLS = "symbols"
        const val KEY = "key"
        const val ON = "on"
        const val NAME = "name"
        const val EMPTY_STRING = ""
        const val DASH = " — "
        const val COLON = ":"
        const val COMMITS = "commits"
        const val EXECUTIONS = "executions"
        const val CODING_TIME = "coding time"
        const val TYPED = "typed"
        const val CONTINUOUSLY = "continuously"
        const val OVERALL = "overall"
        const val TODAY = "today"
        const val ACHIEVED = "achieved"
        const val ACHIEVEMENT = "achievement"
        const val NOTIFICATION_MSG_1 = "$SYMBOLS typed. You are coding for"
        const val NOTIFICATION_MSG_2 = "$HOURS now"

        const val DATE_FORMAT = "dd.MM.yy"

        const val DEFAULT_NOTIFICATION_H = 2
        const val DEFAULT_ENABLED_NOTIFICATIONS = true

        const val MIN_NOTIFICATION_H = 1
        const val MAX_NOTIFICATION_H = 24
        const val STEP_NOTIFICATION_H = 1

        //-----------------api key--------------//
        const val API_KEY_TEXT_FIELD_WIDTH = 30
        const val API_KEY = "API key"
        const val INVALID_API_KEY_MESSAGE = "Invalid $API_KEY"
        const val API_KEY_CONFIRM_MESSAGE = "$API_KEY is invalid. Apply anyway?"
        const val SETTINGS_DIALOG_NAME = "$PLUGIN_NAME $API_KEY"
        const val HELP_ID_API_KEY_DIALOG = "noidle_help_id_api_key_dialog"
        const val HELP_TEXT_API_KEY_DIALOG = "1. Register on <a href=\"https://noidle.herokuapp.com\">https://noidle.herokuapp.com</a>" +
                "<br>" +
                "2. Copy API key from <a href=\"https://noidle.herokuapp.com/profile\">profile page</a>"
        //---------------team------------------//
        const val TEAM = "team"
        const val TEAM_TEXT_FIELD_WIDTH = 30
        const val TEAM_DIALOG_NAME = "$PLUGIN_NAME: your $TEAM name for "
        const val INVALID_TEAM_NAME_MESSAGE = "Invalid team. This project will not be added to it"
        const val DEFAULT_TEAM_SET_MESSAGE = "This project does not belong to any team"
        const val INVALID_TEAM_LINK="https://gitlab.com/endovitskayaV/NoIdle/blob/master/README.md#invalid-team"
        const val HELP_ID_TEAM_DIALOG = "noidle_help_id_team_dialog"
        const val HELP_TEXT_TEAM_DIALOG = "Set if you want to include project in list of team`s projects.<br>" +
                "Optional. Press Cancel for no team<br>" +
                "<a href=\""+ INVALID_TEAM_LINK+"\">Troubleshooting</a>"


        const val DEFAULT_PROJECT_ID = "no_idle_default_project_endovitskayaV"
        const val DEFAULT_TEAM_ID = EMPTY_STRING
        const val DEFAULT_TEAM_NAME = EMPTY_STRING
        val DEFAULT_TEAM = Team()
        val ZERO_UUID = UUID(0L, 0L)

        //--------------key analysis--------------//
        const val ALLOWED_IDLE_MILLIS = 30_000L

        //-------------utils----------//
        const val LESS_THAN_MIN = "less than minute"
        const val SUCCESS_CODE = 0

        //----------defaults-----------//
        const val DEFAULT_LONG = 0L
        const val DEFAULT_INT = 0

        //----------------------------settings---------------------------//
        val ERROR_COLOR = Color.RED
        val INFO_COLOR = Color(222, 194, 24)
        val OK_COLOR = Color(0, 0, 0)

        const val INVALID_URL_MESSAGE = "Invalid URL"

        //-------------------------------------TOOL WINDOW-------------------------------------------//

        //-----------------------progress indicators-------------------//
        const val DOWNLOADING_TEXT = "Downloading NoIdle notifications"
        const val REFRESHING_TEXT = "Refreshing NoIdle data"

        const val FADEOUT_TIME = 10_000L

        val SECTION_MARGIN = BorderFactory.createEmptyBorder(25, 0, 4, 0)
        val MIN_SECTION_MARGIN = BorderFactory.createEmptyBorder(14, 0, 0, 0)

        //---------------------------colors-----------------------------//
        private val PANEL_BACKGROUND_COLOR_LIGHT = Gray._242
        private val BACKGROUND_COLOR_DARK = Color(60, 63, 65)
        val BEIDGE_PANEL_BACKGROUND_COLOR = JBColor(PANEL_BACKGROUND_COLOR_LIGHT, BACKGROUND_COLOR_DARK)
        val WHITE_PANEL_BACKGROUND_COLOR = JBColor(Color.WHITE, BACKGROUND_COLOR_DARK)

        private val BORDER_COLOR_LIGHT = Gray._192
        private val LIGHT_COLOR_DARK_THEME = Color(173, 175, 179)
        val BALLOON_BORDER_COLOR = JBColor(BORDER_COLOR_LIGHT, LIGHT_COLOR_DARK_THEME)

        private val TEXT_COLOR_LIGHT = Color.BLACK
        val TEXT_COLOR = JBColor(TEXT_COLOR_LIGHT, LIGHT_COLOR_DARK_THEME)

        private val PANEL_BORDER_COLOR_LIGHT_FOCUSED = Gray._190
        val PANEL_BORDER_COLOR_FOCUSED = JBColor(PANEL_BORDER_COLOR_LIGHT_FOCUSED, LIGHT_COLOR_DARK_THEME)

        private val LABEL_BACKGROUND_COLOR_LIGHT_FOCUSED = Gray._227
        val LABEL_BACKGROUND_COLOR_FOCUSED = JBColor(LABEL_BACKGROUND_COLOR_LIGHT_FOCUSED, BACKGROUND_COLOR_DARK)

        private val PANEL_BACKGROUND_COLOR_LIGHT_FOCUSED = Color(222, 231, 247)
        private val PANEL_BACKGROUND_COLOR_DARK_FOCUSED = Color(60, 70, 85)
        val PANEL_BACKGROUND_COLOR_FOCUSED = JBColor(PANEL_BACKGROUND_COLOR_LIGHT_FOCUSED, PANEL_BACKGROUND_COLOR_DARK_FOCUSED)

        private val BALLOON_BACKGROUND_COLOR_LIGHT = Color(252, 254, 202)
        val BALLOON_BACKGROUND_COLOR = JBColor(BALLOON_BACKGROUND_COLOR_LIGHT, BACKGROUND_COLOR_DARK)
        //-----------------------------------------------------------------------------------------------------------------//
    }
}