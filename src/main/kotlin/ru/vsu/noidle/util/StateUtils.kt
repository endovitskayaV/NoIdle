package ru.vsu.noidle.util

import com.intellij.openapi.project.Project
import com.intellij.util.containers.stream
import ru.vsu.noidle.model.dto.StatisticsSubType
import ru.vsu.noidle.model.dto.StatisticsType
import ru.vsu.noidle.sender.GetUser
import ru.vsu.noidle.sender.Sender
import ru.vsu.noidle.state.PluginState
import ru.vsu.noidle.state.Statistics
import java.util.*
import java.util.function.Consumer

class StateUtils {
    companion object {
        @Volatile
        private var INSTANCE: StateUtils? = null

        fun getInstance(): StateUtils =
                INSTANCE ?: synchronized(this) {
                    INSTANCE ?: StateUtils().also { INSTANCE = it }
                }
    }


    private var timer = Timer()

    /**
     * Saves email in state and updates every [Constants.GET_FROM_SERVER_LONG_PERIOD] millis
     */
    fun setEmail() {
        //get email from server
        val user = Sender.getUser(PluginState.getInstance().apiKey)
        if (user==null) { //can not get email from server
            //try again soon
            schedule(
                    GetUser(), Constants.GET_FROM_SERVER_MIN_DELAY, Constants.GET_FROM_SERVER_MIN_PERIOD
            )
        } else { //set info and check for email update much later
            PluginState.getInstance().email = user.email
            PluginState.getInstance().userName=user.name
            schedule(
                    GetUser(), Constants.GET_FROM_SERVER_LONG_DELAY, Constants.GET_FROM_SERVER_LONG_PERIOD
            )
        }
    }

    fun isValidUUID(uuid: String): Boolean {
        try {
            UUID.fromString(uuid) //check if is UUID
        } catch (e: IllegalArgumentException) {
            return false
        }

        return true
    }

    fun isValidTeamId(teamName: String, project: String?): Boolean? {
        val teamFromServer = Sender.getTeamByName(teamName)
        val teamFromState = PluginState.getInstance().getTeamByProjectPath(project)
        if (teamFromServer == null) {
            return false
        }
        if (teamFromState == null || teamFromState == Constants.DEFAULT_TEAM) {
            return true
        }
        return if (teamFromState.id.equals(teamFromServer.id)) true else null
    }

    fun setTeamForCurrentProject(teamName: String, project: Project?) {
        val projectId = ProjectUtils.getProjectId(project) ?: return
        removeProjectFromState(projectId)
        addProjectToNewTeam(teamName, projectId)
    }


    private fun removeProjectFromState(projectId: String) {
        PluginState.getInstance().teams.entries.stream().forEach { entry ->
            entry.value.projects.removeIf { projIdI -> projectId.equals(projIdI) }
        }
    }

    private fun addProjectToNewTeam(teamName: String, projectId: String) {
        val state = PluginState.getInstance()
        val newTeam = Sender.getTeamByName(teamName) ?: Constants.DEFAULT_TEAM
        val teamInState = state.teams.entries.stream()
                .filter { entry -> entry.key.equals(newTeam) }.findFirst().orElse(null)
        if (teamInState == null) {
            state.teams[newTeam] = Statistics(projects = mutableSetOf(projectId))
        } else {
            teamInState.value.projects.add(projectId)
        }
    }

    fun updateStatistics(statistics: Statistics, consumer: Consumer<Statistics>) {
        consumer.accept(statistics)
        val defaultStatistics = PluginState.getInstance().getOverallDefaultStatistics()
        if (!statistics.equals(defaultStatistics)) {
            consumer.accept(defaultStatistics)
        }
    }

    fun updateTeamName(id: String, newName: String) {
        PluginState.getInstance().getTeamById(id)?.name = newName
    }

    fun isValidUrl(scheme: String?, host: String?) = scheme != null && host != null && Sender.validUrl(scheme, host)


    fun refreshStateFromServer(teamId: String) {
        val pluginStatistics = PluginState.getInstance().getOverallDefaultStatistics()
        val serverStatistics = Sender.getStatistics(PluginState.getInstance().apiKey, teamId) ?: return
        serverStatistics.stream()
                .filter({ statistics ->
                    statistics.type != null
                            && statistics.subType != null
                            && statistics.value != null
                })
                .forEach {
                    when (it.type) {
                        StatisticsType.COMMIT -> {
                            when (it.subType) {
                                StatisticsSubType.PER_LIFE -> {
                                    if (Constants.SUCCESSFUL.equals(it.extraValue)
                                            && pluginStatistics.successfulCommitsPerLife < it.value) {
                                        pluginStatistics.successfulCommitsPerLife = it.value
                                    } else if (Constants.FAILED.equals(it.extraValue)
                                            && pluginStatistics.failedCommitsPerLife < it.value) {
                                        pluginStatistics.failedCommitsPerLife = it.value
                                    }
                                }
                                StatisticsSubType.PER_DAY -> {
                                    if (Constants.SUCCESSFUL.equals(it.extraValue)
                                            && pluginStatistics.successfulCommitsPerDay < it.value) {
                                        pluginStatistics.successfulCommitsPerDay = it.value
                                    } else if (Constants.FAILED.equals(it.extraValue)
                                            && pluginStatistics.failedCommitsPerDay < it.value) {
                                        pluginStatistics.failedCommitsPerDay = it.value
                                    }
                                }
                            }
                        }
                        StatisticsType.EXEC -> {
                            when (it.subType) {
                                StatisticsSubType.PER_LIFE -> {
                                    if (Constants.SUCCESSFUL.equals(it.extraValue)
                                            && pluginStatistics.successfulExecutionsPerLife < it.value) {
                                        pluginStatistics.successfulExecutionsPerLife = it.value
                                    } else if (Constants.FAILED.equals(it.extraValue)
                                            && pluginStatistics.failedExecutionsPerLife < it.value) {
                                        pluginStatistics.failedExecutionsPerLife = it.value
                                    }
                                }
                                StatisticsSubType.PER_DAY -> {
                                    if (Constants.SUCCESSFUL.equals(it.extraValue)
                                            && pluginStatistics.successfulExecutionsPerDay < it.value) {
                                        pluginStatistics.successfulExecutionsPerDay = it.value
                                    } else if (Constants.FAILED.equals(it.extraValue)
                                            && pluginStatistics.failedExecutionsPerDay < it.value) {
                                        pluginStatistics.failedExecutionsPerDay = it.value
                                    }
                                }
                            }
                        }
                        StatisticsType.TIME -> {
                            when (it.subType) {
                                StatisticsSubType.PER_LIFE -> {
                                    if (pluginStatistics.timePerLife < it.value) {
                                        pluginStatistics.timePerLife = it.value
                                    }
                                }
                                StatisticsSubType.PER_DAY -> {
                                    if (pluginStatistics.timePerDay < it.value) {
                                        pluginStatistics.timePerDay = it.value
                                    }
                                }
                                StatisticsSubType.CONTINUOUS_PER_LIFE -> {
                                    if (pluginStatistics.maxContinuousTimePerLife < it.value) {
                                        pluginStatistics.maxContinuousTimePerLife = it.value
                                    }
                                }
                                StatisticsSubType.CONTINUOUS_PER_DAY -> {
                                    if (pluginStatistics.maxContinuousTimePerDay < it.value) {
                                        pluginStatistics.maxContinuousTimePerDay = it.value
                                    }
                                }
                            }
                        }
                        StatisticsType.SYMBOL -> {
                            when (it.subType) {
                                StatisticsSubType.PER_LIFE -> {
                                    if (pluginStatistics.symbolsQuantityPerLife < it.value) {
                                        pluginStatistics.symbolsQuantityPerLife = it.value
                                    }
                                }
                                StatisticsSubType.PER_DAY -> {
                                    if (pluginStatistics.symbolsQuantityPerDay < it.value) {
                                        pluginStatistics.symbolsQuantityPerDay = it.value
                                    }
                                }
                                StatisticsSubType.CONTINUOUS_PER_LIFE -> {
                                    if (pluginStatistics.maxContinuousSymbolsPerLife < it.value) {
                                        pluginStatistics.maxContinuousSymbolsPerLife = it.value
                                    }
                                }
                                StatisticsSubType.CONTINUOUS_PER_DAY -> {
                                    if (pluginStatistics.maxContinuousSymbolsPerDay < it.value) {
                                        pluginStatistics.maxContinuousSymbolsPerDay = it.value
                                    }
                                }
                            }
                        }
                        StatisticsType.LANG_TIME -> {
                            when (it.subType) {
                                StatisticsSubType.PER_LIFE -> {
                                    val languageInfo = pluginStatistics.languageInfo[it.extraValue]
                                    if (languageInfo != null && languageInfo.timePerLife < it.value) {
                                        languageInfo.timePerLife = it.value
                                    }
                                }
                                StatisticsSubType.PER_DAY -> {
                                    val languageInfo = pluginStatistics.languageInfo[it.extraValue]
                                    if (languageInfo != null && languageInfo.timePerDay < it.value) {
                                        languageInfo.timePerDay = it.value
                                    }
                                }
                            }
                        }
                        StatisticsType.LANG_SYMBOL -> {
                            when (it.subType) {
                                StatisticsSubType.PER_LIFE -> {
                                    val languageInfo = pluginStatistics.languageInfo[it.extraValue]
                                    if (languageInfo != null && languageInfo.symbolsPerLife < it.value) {
                                        languageInfo.symbolsPerLife = it.value
                                    }
                                }
                                StatisticsSubType.PER_DAY -> {
                                    val languageInfo = pluginStatistics.languageInfo[it.extraValue]
                                    if (languageInfo != null && languageInfo.symbolsPerDay < it.value) {
                                        languageInfo.symbolsPerDay = it.value
                                    }
                                }
                            }
                        }
                        StatisticsType.SINGLE_KEY -> {
                            when (it.subType) {
                                StatisticsSubType.PER_LIFE -> {
                                    val keyInfo = pluginStatistics.keyTapQuantity[it.extraValue]
                                    if (keyInfo != null && keyInfo.perLife < it.value) {
                                        keyInfo.perLife = it.value
                                    }
                                }
                                StatisticsSubType.PER_DAY -> {
                                    val keyInfo = pluginStatistics.keyTapQuantity[it.extraValue]
                                    if (keyInfo != null && keyInfo.perDay < it.value) {
                                        keyInfo.perDay = it.value
                                    }
                                }
                            }
                        }
                    }
                }
    }

    /**
     * Creates new timer task
     */
    private fun schedule(task: TimerTask, delay: Long, period: Long) {
        timer.cancel()
        timer = Timer()
        timer.schedule(task, delay, period)
    }
}