package ru.vsu.noidle.analyser

import com.intellij.openapi.project.Project
import com.intellij.openapi.project.ProjectManager
import com.intellij.openapi.project.ProjectManagerListener
import ru.vsu.noidle.state.PluginState
import ru.vsu.noidle.ui.dialog.TeamDialog
import ru.vsu.noidle.util.Constants
import ru.vsu.noidle.util.ProjectUtils

class TeamSetter {
    companion object {
        private var state = PluginState.getInstance()

        fun set() {
            //get project open event
            ProjectManager.getInstance().addProjectManagerListener(object : ProjectManagerListener {

                override fun projectOpened(project: Project) {
                    if (!state.teamSpecified(project)) {
                        if (!state.apiKey.isEmpty()) {
                            TeamDialog(project).show() //show team dialog if api key was saved
                        } else {
                            ProjectUtils.getProjectId(project)?.let { state.teams[Constants.DEFAULT_TEAM]?.projects?.add(it) }
                        }
                    }
                }
            })
        }
    }
}