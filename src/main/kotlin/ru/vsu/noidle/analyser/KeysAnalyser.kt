package ru.vsu.noidle.analyser

import com.intellij.openapi.editor.impl.EditorComponentImpl
import com.intellij.openapi.project.Project
import com.intellij.openapi.util.IconLoader
import org.apache.commons.lang3.time.DateUtils
import ru.vsu.noidle.model.Language
import ru.vsu.noidle.state.PluginState
import ru.vsu.noidle.state.Statistics
import ru.vsu.noidle.util.Constants
import ru.vsu.noidle.util.NotificationUtils
import ru.vsu.noidle.util.ProjectUtils
import ru.vsu.noidle.util.StateUtils
import java.awt.event.KeyEvent
import java.util.*
import java.util.concurrent.TimeUnit
import java.util.function.Consumer

/**
 * Calculates statistics about time and keys per day/life continuously (and not) by languages (and not)
 */
class KeysAnalyser private constructor() {

    companion object {

        @Volatile
        private var INSTANCE: KeysAnalyser? = null

        fun getInstance(): KeysAnalyser =
                INSTANCE ?: synchronized(this) {
                    INSTANCE ?: KeysAnalyser().also { INSTANCE = it }
                }
    }

    var currentContinuousTypingTime: Long = Constants.DEFAULT_LONG
        private set

    var currentContinuousTypedSymbols: Long = Constants.DEFAULT_LONG
        private set

    //TAP {timeBetweenTaps} TAP
    //if timeBetweenTaps> ALLOWED_IDLE_MILLIS timeBetweenTaps=ALLOWED_IDLE_MILLIS
    //overallTime = SUM (timeBetweenTaps)
    private var timeBetweenTaps = Constants.DEFAULT_LONG

    private var state = PluginState.getInstance()

    private var statistics = state.getOverallDefaultStatistics()

    var lastTapTime = Constants.DEFAULT_LONG
        private set

    /**
     * main
     */
    fun calculateKeyInfo(event: KeyEvent) {
        val project = getProject(event)
        statistics = state.getStatistics(ProjectUtils.getProjectId(project)) ?: state.getOverallDefaultStatistics()

        //hold time when user typed
        val now = System.currentTimeMillis()
        lastTapTime = now

        //first time
        if (timeBetweenTaps == Constants.DEFAULT_LONG) {
            timeBetweenTaps = now
        }

        //next day
        if (!isToday()) {
            currentContinuousTypingTime = Constants.DEFAULT_LONG
            currentContinuousTypedSymbols = Constants.DEFAULT_LONG
            state.resetStateToNextDay()
        }

        //typing time
        timeBetweenTaps = now - timeBetweenTaps

        //correct typing time
        if (timeBetweenTaps > Constants.ALLOWED_IDLE_MILLIS) {
            timeBetweenTaps = Constants.ALLOWED_IDLE_MILLIS

            //continuous typing stopped
            updateStatistics(Consumer { calculateMaxContinuousTime(it) })
            updateStatistics(Consumer { calculateMaxContinuousSymbols(it) })
            currentContinuousTypedSymbols++
        } else {  //continuous typing
            currentContinuousTypingTime += timeBetweenTaps
            currentContinuousTypedSymbols++
        }

        updateStatistics(Consumer { it.timePerDay += timeBetweenTaps })
        updateStatistics(Consumer { it.timePerLife += timeBetweenTaps })

        updateStatistics(Consumer { it.symbolsQuantityPerDay++ })
        updateStatistics(Consumer { it.symbolsQuantityPerLife++ })

        updateStatistics(Consumer { increaseKeyTapCounter(KeyEvent.getKeyText(event.keyCode), it) })
        updateStatistics(Consumer { updateLanguageInfo(it,event) })

        showNotification(project)
        timeBetweenTaps = System.currentTimeMillis()

        printInfo(event)
    }

    private fun updateStatistics(consumer: Consumer<Statistics>) {
        StateUtils.getInstance().updateStatistics(statistics, consumer)
    }

    fun calculateAndSetContinuous(statistics: Statistics) {
        val now = System.currentTimeMillis()
        timeBetweenTaps = now - timeBetweenTaps
        if (timeBetweenTaps <= Constants.ALLOWED_IDLE_MILLIS) {
            currentContinuousTypingTime += timeBetweenTaps
        }
        calculateMaxContinuousTime(statistics)
        calculateMaxContinuousSymbols(statistics)
    }

    private fun calculateMaxContinuousTime(statistics: Statistics) {
        //continuous typing stopped
        if (currentContinuousTypingTime > statistics.maxContinuousTimePerDay) {
            statistics.maxContinuousTimePerDay = currentContinuousTypingTime
        }
        if (currentContinuousTypingTime > statistics.maxContinuousTimePerLife) {
            statistics.maxContinuousTimePerLife = currentContinuousTypingTime
        }
        currentContinuousTypingTime = Constants.DEFAULT_LONG
    }

    private fun calculateMaxContinuousSymbols(statistics: Statistics) {
        //continuous typing stopped
        if (currentContinuousTypedSymbols > statistics.maxContinuousSymbolsPerDay) {
            statistics.maxContinuousSymbolsPerDay = currentContinuousTypedSymbols
        }
        if (currentContinuousTypedSymbols > statistics.maxContinuousSymbolsPerLife) {
            statistics.maxContinuousSymbolsPerLife = currentContinuousTypedSymbols
        }
        currentContinuousTypedSymbols = Constants.DEFAULT_LONG
    }

    private fun isToday(): Boolean {
        val today = DateUtils.toCalendar(Date(state.today))
        return (Calendar.getInstance().get(Calendar.YEAR) == (today.get(Calendar.YEAR)) &&
                Calendar.getInstance().get(Calendar.DAY_OF_YEAR) == (today.get(Calendar.DAY_OF_YEAR)))
    }

    private fun increaseKeyTapCounter(name: String, statistics: Statistics) {
        val keyInfo = statistics.keyTapQuantity[name]
        if (keyInfo != null) {
            keyInfo.perDay++
            keyInfo.perLife++
        }
    }

    private fun updateLanguageInfo(statistics: Statistics, event: KeyEvent) {
        val source = event.source

        if (source is EditorComponentImpl && source.editor.virtualFile != null) {
            val extension = source.editor.virtualFile.extension
            val name = Language.byExtension(extension).shortcut
            val timeToAdd: Long = if (state.previousFileExt.equals(extension) || state.previousFileExt.isEmpty()) timeBetweenTaps else 0
            val languageInfo = statistics.languageInfo[name]
            if (languageInfo != null) {
                languageInfo.timePerDay += timeToAdd
                languageInfo.timePerLife += timeToAdd
                languageInfo.symbolsPerDay++
                languageInfo.symbolsPerLife++
            }
        }
    }

    private fun getProject(event: KeyEvent): Project? = ProjectUtils.getProject(event.component)

    /**
     * shows notification about typed time and symbols
     */
    private fun showNotification(project: Project?) {
        val sumStatistics = PluginState.getInstance().getOverallDefaultStatistics()

        val alreadyTypedHours = TimeUnit.MILLISECONDS.toHours(sumStatistics.timePerDay)
        if (alreadyTypedHours > state.lastNotificationHours - 1 + state.notificationFrequencyHours) {
            state.lastNotificationHours = alreadyTypedHours.toInt()

            if (state.enabledReportNotifications) {
                NotificationUtils.getInstance().showNotification(
                        IconLoader.getIcon(Constants.NOTIFICATION_ICON_PATH),
                        Constants.ACHIEVEMENT_NOTIFICATION_TITLE,
                        Constants.ACHIEVEMENT_NOTIFICATION_SUB_TITLE,
                        "${sumStatistics.symbolsQuantityPerDay} ${Constants.NOTIFICATION_MSG_1} $alreadyTypedHours ${Constants.NOTIFICATION_MSG_2}",
                        project
                )
            }
        }
    }

    //info for me :-)
    private fun printInfo(event: KeyEvent) {
        println("----------------- key event -----------------")
        println("event.id: ${event.id} ${typeOf(event)}")
        println("event.isShiftDown:  ${event.isShiftDown}")
        println("event.isActionKey: ${event.isActionKey}")
        println("event.keyChar: ${event.keyChar}")
        println("key text: ${KeyEvent.getKeyText(event.keyCode)}")
        println("event.keyCode: ${event.keyCode}")
        println("event.extendedKeyCode: ${event.extendedKeyCode}")
        println("event.isShiftDown: ${event.isShiftDown}")
        println("event.isAltDown: ${event.isAltDown}")
        println("event.isControlDown: ${event.isControlDown}")
        println("event.isMetaDown: ${event.isMetaDown}")
        println("event.isAltGraphDown: ${event.isAltGraphDown}")
        println("event.keyLocation: ${event.keyLocation}")
        println("event.source: ${event.source.javaClass}")
    }

    private fun typeOf(event: KeyEvent): String {
        return when (event.id) {
            KeyEvent.KEY_TYPED -> "KEY_TYPED"
            KeyEvent.KEY_PRESSED -> "KEY_PRESSED"
            KeyEvent.KEY_RELEASED -> "KEY_RELEASED"
            else -> "<other>"
        }
    }
}