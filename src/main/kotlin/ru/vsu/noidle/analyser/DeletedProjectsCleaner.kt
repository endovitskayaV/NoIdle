package ru.vsu.noidle.analyser

import ru.vsu.noidle.state.PluginState
import ru.vsu.noidle.util.Constants
import java.io.File
import java.util.*

class DeletedProjectsCleaner : TimerTask() {

    override fun run() {
        PluginState.getInstance().teams.forEach {
            it.value.projects.removeIf { project -> !File(project).exists() && !(project.equals(Constants.DEFAULT_PROJECT_ID) && it.key.equals(Constants.DEFAULT_TEAM)) }
        }
        PluginState.getInstance().teams.entries.removeIf { entry -> entry.value.projects.isEmpty() && !entry.key.equals(Constants.DEFAULT_TEAM) }
    }
}