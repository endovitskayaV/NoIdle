package ru.vsu.noidle.analyser

import com.intellij.execution.Executor
import com.intellij.execution.process.ProcessEvent
import com.intellij.execution.process.ProcessListener
import com.intellij.execution.ui.RunContentDescriptor
import com.intellij.execution.ui.RunContentManager
import com.intellij.execution.ui.RunContentWithExecutorListener
import com.intellij.openapi.project.Project
import com.intellij.openapi.project.ProjectManager
import com.intellij.openapi.project.ProjectManagerListener
import com.intellij.openapi.util.Key
import ru.vsu.noidle.state.PluginState
import ru.vsu.noidle.util.Constants
import ru.vsu.noidle.util.ProjectUtils

/**
 * Calculates quantity of successful and unsuccessful program runs
 */
class ExitStatusAnalyser {
    companion object {
        private var state = PluginState.getInstance()

        fun analyse() {
            //get project open event
            ProjectManager.getInstance().addProjectManagerListener(object : ProjectManagerListener {

                override fun projectOpened(project: Project?) {

                    //get execution event
                    project?.messageBus?.connect()?.subscribe(RunContentManager.TOPIC, object : RunContentWithExecutorListener {

                        override fun contentSelected(descriptor: RunContentDescriptor?, executor: Executor) {
                            descriptor?.processHandler?.addProcessListener(object : ProcessListener {

                                override fun onTextAvailable(event: ProcessEvent, outputType: Key<*>) {
                                }

                                override fun processWillTerminate(event: ProcessEvent, willBeDestroyed: Boolean) {
                                }

                                override fun startNotified(event: ProcessEvent) {
                                }

                                override fun processTerminated(event: ProcessEvent) {
                                    //https://ru.stackoverflow.com/questions/738549/process-finished-with-exit-code-1
                                    val statistics = state.getStatistics(ProjectUtils.getProjectId(project)) ?: return;
                                    if (event.exitCode > Constants.SUCCESS_CODE) {
                                        statistics.failedExecutionsPerLife++
                                        statistics.failedExecutionsPerDay++
                                    } else {
                                        statistics.successfulExecutionsPerLife++
                                        statistics.successfulExecutionsPerDay++
                                    }
                                }
                            })
                        }

                        override fun contentRemoved(descriptor: RunContentDescriptor?, executor: Executor) {
                        }

                    })
                }

            })
        }
    }
}