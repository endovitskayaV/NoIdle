package ru.vsu.noidle.analyser

import com.intellij.openapi.vcs.CheckinProjectPanel
import com.intellij.openapi.vcs.VcsException
import com.intellij.openapi.vcs.changes.CommitContext
import com.intellij.openapi.vcs.checkin.CheckinHandler
import com.intellij.openapi.vcs.checkin.CheckinHandlerFactory
import ru.vsu.noidle.state.PluginState
import ru.vsu.noidle.util.ProjectUtils


/**
 * Calculates quantity of successful and unsuccessful commits
 */
class CommitsAnalyser : CheckinHandlerFactory() {

    private val state = PluginState.getInstance()

    override fun createHandler(panel: CheckinProjectPanel, commitContext: CommitContext): CheckinHandler {
        return object : CheckinHandler() {
            override fun checkinFailed(exception: MutableList<VcsException>?) {
                val statistics = state.getStatistics(ProjectUtils.getProjectId(panel.project))
                if (statistics != null) {
                    statistics.failedCommitsPerLife++
                    statistics.failedCommitsPerDay++
                }
            }

            override fun checkinSuccessful() {
                val statistics = state.getStatistics(ProjectUtils.getProjectId(panel.project))
                if (statistics != null) {
                    statistics.successfulCommitsPerLife++
                    statistics.successfulCommitsPerDay++
                }
            }
        }
    }
}