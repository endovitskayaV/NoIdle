package ru.vsu.noidle.component

import com.intellij.ide.AppLifecycleListener
import com.intellij.openapi.application.ApplicationManager
import com.intellij.openapi.components.ApplicationComponent
import com.intellij.openapi.project.ProjectManager
import ru.vsu.noidle.analyser.DeletedProjectsCleaner
import ru.vsu.noidle.analyser.ExitStatusAnalyser
import ru.vsu.noidle.analyser.KeysAnalyser
import ru.vsu.noidle.analyser.TeamSetter
import ru.vsu.noidle.sender.SendStatisticsGetNotifications
import ru.vsu.noidle.state.PluginState
import ru.vsu.noidle.ui.dialog.ApiKeyDialog
import ru.vsu.noidle.util.Constants.Companion.REMOVE_DELAY
import ru.vsu.noidle.util.Constants.Companion.REMOVE_PERIOD
import ru.vsu.noidle.util.Constants.Companion.SEND_TO_SERVER_DELAY
import ru.vsu.noidle.util.Constants.Companion.SEND_TO_SERVER_PERIOD
import ru.vsu.noidle.util.ProjectUtils
import java.awt.AWTEvent
import java.awt.Toolkit
import java.awt.event.KeyEvent
import java.util.*

/**
 * Can be approximately named as main class for this very plugin
 */
class ApplicationComponentImpl : ApplicationComponent {

    /**
     * Can be approximately named as main method for this very plugin
     */
    override fun initComponent() {
        addIdeEventsListener()
        Timer().schedule(SendStatisticsGetNotifications(), SEND_TO_SERVER_DELAY, SEND_TO_SERVER_PERIOD)
        showApiKeyDialog()
        addKeyListener()
        ExitStatusAnalyser.analyse()
        TeamSetter.set()
        Timer().schedule(DeletedProjectsCleaner(), REMOVE_DELAY, REMOVE_PERIOD)
    }

    /**
     * saves continuous time and symbols before ide closes
     */
    private fun addIdeEventsListener() {
        ApplicationManager.getApplication().messageBus.connect()
                .subscribe(AppLifecycleListener.TOPIC, object : AppLifecycleListener {

                    override fun appClosing() {
                        ProjectManager.getInstance().openProjects.forEach {
                            val statistics = PluginState.getInstance().getStatistics(ProjectUtils.getProjectId(it))
                            statistics?.let { statist -> KeysAnalyser.getInstance().calculateAndSetContinuous(statist) }
                        }

                        SendStatisticsGetNotifications.doGetNotifications = false
                        SendStatisticsGetNotifications().run()
                    }
                })
    }

    /**
     * If api key is not set
     * then shows dialog to set it
     * then if api key is set shows dialog to set team
     */
    private fun showApiKeyDialog() {
        ApplicationManager.getApplication().invokeLater {

            if (PluginState.getInstance().apiKey.isEmpty()) {
                val project = ProjectManager.getInstance().defaultProject
                val apiKey = ApiKeyDialog(project)
                apiKey.show()
            }
        }
    }

    private fun addKeyListener() {
        Toolkit.getDefaultToolkit().addAWTEventListener({ event ->

            //not other states, because all keys have RELEASED state,
            // but not all of them have ie PRESSED (ie backspace or enter)
            if ((event is KeyEvent) && isReleasedEvent(event)) {

                KeysAnalyser.getInstance().calculateKeyInfo(event)
            }
        }, AWTEvent.KEY_EVENT_MASK)
    }

    private fun isReleasedEvent(event: KeyEvent): Boolean {
        return event.id == KeyEvent.KEY_RELEASED
    }

}