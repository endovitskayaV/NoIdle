package ru.vsu.noidle.sender

import ru.vsu.noidle.util.Constants
import ru.vsu.noidle.util.StateUtils
import java.util.*

/**
 * Saves user email(got from server) in state
 * Updates email every [Constants.GET_FROM_SERVER_LONG_PERIOD] millis
 */
class GetUser : TimerTask() {

    override fun run() {
        StateUtils.getInstance().setEmail();
    }
}