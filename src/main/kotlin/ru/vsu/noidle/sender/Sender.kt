package ru.vsu.noidle.sender

import com.google.gson.GsonBuilder
import org.apache.http.client.entity.EntityBuilder
import org.apache.http.client.methods.CloseableHttpResponse
import org.apache.http.client.methods.HttpGet
import org.apache.http.client.methods.HttpPost
import org.apache.http.client.methods.HttpRequestBase
import org.apache.http.client.utils.URIBuilder
import org.apache.http.entity.ContentType
import org.apache.http.impl.client.HttpClients
import org.apache.http.message.BasicNameValuePair
import ru.vsu.noidle.model.Team
import ru.vsu.noidle.model.dto.*
import ru.vsu.noidle.state.PluginState
import ru.vsu.noidle.util.Constants
import ru.vsu.noidle.util.Constants.Companion.ADD
import ru.vsu.noidle.util.Constants.Companion.CHECK
import ru.vsu.noidle.util.Constants.Companion.EMPTY_STRING
import ru.vsu.noidle.util.Constants.Companion.NOTIFICATIONS
import ru.vsu.noidle.util.Constants.Companion.SAVE
import ru.vsu.noidle.util.Constants.Companion.SHORT
import ru.vsu.noidle.util.Constants.Companion.STATISTICS
import ru.vsu.noidle.util.Constants.Companion.TEAMS
import ru.vsu.noidle.util.Constants.Companion.TEAM_ID_PARAM
import ru.vsu.noidle.util.Constants.Companion.TEAM_NAME_PARAM
import ru.vsu.noidle.util.Constants.Companion.TYPES_PARAM
import ru.vsu.noidle.util.Constants.Companion.USERS
import ru.vsu.noidle.util.Constants.Companion.USER_ID_PARAM
import ru.vsu.noidle.util.StateUtils
import java.io.InputStreamReader
import java.util.*

/**
 * Http requests handler
 */
class Sender {
    companion object {
        private var state = PluginState.getInstance()

        /**
         * Sends statistics to server and checks for notifications
         */
        fun sendStatistics(statisticsList: List<StatisticsDto>, userId: String,
                           team: Team?, types: List<AchievementType>?): Array<NotificationDto>? {

            val teamId = if (team == null) Constants.EMPTY_STRING else team.id

            val query = HttpPost(
                    URIBuilder("${state.scheme}://${state.host}$STATISTICS$SAVE")
                            .addParameters(listOf(
                                    BasicNameValuePair(USER_ID_PARAM, userId),
                                    BasicNameValuePair(TEAM_ID_PARAM, teamId)
                            ))
                            .build()
            )

            query.entity = EntityBuilder.create()
                    .setText(GsonBuilder().create().toJson(statisticsList))
                    .setContentType(ContentType.APPLICATION_JSON)
                    .build()

            val isSuccess = success(getResponse(query))

            return if (SendStatisticsGetNotifications.doGetNotifications && isSuccess == true && types != null)
                getNotifications(userId, types)
            else null
        }

        private fun getNotifications(userId: String, types: List<AchievementType>): Array<NotificationDto>? {
            return doSend(
                    HttpGet(URIBuilder()
                            .setScheme(state.scheme)
                            .setHost(state.host)
                            .setPath(NOTIFICATIONS)
                            .setParameters(listOf(
                                    BasicNameValuePair(USER_ID_PARAM, userId),
                                    BasicNameValuePair(TYPES_PARAM, types.joinToString())
                            ))
                            .build()),
                    Array<NotificationDto>::class.java)
        }

        fun getUser(userId: String): UserDto? {
            return doSend(
                    HttpGet(URIBuilder()
                            .setScheme(state.scheme)
                            .setHost(state.host)
                            .setPath(USERS + userId)
                            .build()),
                    UserDto::class.java)
        }

        fun getTeamByName(name: String?): Team? {
            if (name == null || name.isEmpty()) {
                return null
            }

            //for debug
            val query = HttpGet(
                    URIBuilder("${state.scheme}://${state.host}$TEAMS$SHORT")
                            .addParameters(listOf(BasicNameValuePair(TEAM_NAME_PARAM, removeSpaces(name))))
                            .build()
            )
            val teamDto = doSend(query, TeamDto::class.java)
            return if (teamDto == null) null else Team(teamDto.id.toString(), teamDto.name)
        }

        fun validUrl(scheme: String, host: String): Boolean {

            //for debug
            val query = HttpGet("$scheme://$host$STATISTICS$CHECK")

            val isSuccess = success(getResponse(query))

            return isSuccess ?: false
        }

        fun getTeamById(id: String): Team? {
            if (id.isEmpty()) {
                return null
            }

            //for debug
            val query = HttpGet(URIBuilder("${state.scheme}://${state.host}$TEAMS$SHORT/$id").build())
            val teamDto = doSend(query, TeamDto::class.java)
            return if (teamDto == null || (teamDto.id.equals(UUID(0L, 0L)) && teamDto.name.equals(EMPTY_STRING)))
                null
            else Team(teamDto.id.toString(), teamDto.name)
        }

        fun checkIfAddedUserTeam(userId: UUID, teamName: String?): Boolean {
            val team = getTeamByName(teamName) ?: return false
            //for debug
            val query = HttpPost("${state.scheme}://${state.host}$USERS$userId$TEAMS$CHECK$ADD${team.id}")

            val isSuccess = success(getResponse(query))

            return isSuccess?:false
        }

        fun resetName(team: Team?) {
            if (team == null) {
                return
            }

            val serverTeam = getTeamByName(team.name)

            if (serverTeam == null) {
                val teamFromServer = getTeamById(team.id)

                if (teamFromServer != null) {
                    if (!teamFromServer.name.equals(team.name)) {
                        StateUtils.getInstance().updateTeamName(team.id, teamFromServer.name)
                    }
                }
            }
        }

        fun getStatistics(userId: String, teamId: String): Array<StatisticsDto>? {
            return doSend(HttpGet(
                    URIBuilder("${state.scheme}://${state.host}$STATISTICS")
                            .addParameters(listOf(
                                    BasicNameValuePair(USER_ID_PARAM, userId),
                                    BasicNameValuePair(TEAM_ID_PARAM, teamId)
                            ))
                            .build()
            ),
                    Array<StatisticsDto>::class.java)
        }

        private fun <T> doSend(query: HttpRequestBase, clazz: Class<T>): T? {
            var response: CloseableHttpResponse? = null
            return try {
                response = getResponse(query, false)
                if (response == null) {
                    return null
                }

                val entity = response.entity
                val reader = InputStreamReader(entity.content, ContentType.getOrDefault(entity).charset)
                val result = GsonBuilder().create().fromJson(reader, clazz)
                result
            } catch (e: Exception) {
                println("$e")
                null
            } finally {
                response?.close()
            }
        }

        /**
         * Checks if http response is [Constants.OK] or [Constants.NO_CONTENT]
         * @return null if response is null (exception while handling)
         *         true if response is [Constants.OK] or [Constants.NO_CONTENT]
         *         false otherwise
         */
        private fun success(response: CloseableHttpResponse?): Boolean? {
            val status = response?.statusLine?.statusCode
            return if (status == null)
                null
            else (status == Constants.OK || status == Constants.NO_CONTENT)
        }

        private fun getResponse(query: HttpRequestBase, closeConnetion: Boolean = true): CloseableHttpResponse? {
            val httpClient = HttpClients.createDefault()

            //for debug
            var response: CloseableHttpResponse? = null

            return try {
                response = httpClient.execute(query)
                response
            } catch (e: Exception) {
                println("$e")
                null
            } finally {
                if (closeConnetion) {
                    response?.close()
                }
            }
        }

        private fun removeSpaces(name: String) = name.replace(Constants.SPACE, Constants.SPACE_REPLACEMENT)
    }
}
