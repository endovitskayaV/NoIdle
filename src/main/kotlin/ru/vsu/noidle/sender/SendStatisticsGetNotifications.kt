package ru.vsu.noidle.sender

import ru.vsu.noidle.model.ContinuousType
import ru.vsu.noidle.model.Team
import ru.vsu.noidle.model.dto.AchievementType
import ru.vsu.noidle.model.dto.StatisticsDto
import ru.vsu.noidle.model.dto.StatisticsSubType
import ru.vsu.noidle.model.dto.StatisticsType
import ru.vsu.noidle.state.PluginState
import ru.vsu.noidle.state.Statistics
import ru.vsu.noidle.util.Constants
import ru.vsu.noidle.util.NotificationUtils
import ru.vsu.noidle.util.TimeUtils
import java.util.*
import java.util.function.Function
import java.util.stream.Collectors
import kotlin.collections.ArrayList


/**
 * Sends statistics to server, gets notifications and shows them
 */
class SendStatisticsGetNotifications : TimerTask() {
    companion object {
        var doGetNotifications = true
    }

    private val state = PluginState.getInstance()
    private var dataToSend: MutableList<StatisticsDto> = ArrayList()
    private lateinit var continuous: Map<ContinuousType, Long>

    override fun run() {
        if (state.apiKey.isEmpty()) {
            return
        }

        val teams: MutableMap<Team, Statistics> =
                state.teams.entries.stream()
                        .filter { entry -> needToSend(entry) }
                        .collect(Collectors.toMap(
                                Function<MutableMap.MutableEntry<Team, Statistics>, Team> { t -> t.key },
                                Function<MutableMap.MutableEntry<Team, Statistics>, Statistics> { t -> t.value })).toMutableMap()
        teams.forEach { team, statistics ->
            continuous = TimeUtils.getInstance().getContinuous(statistics)
            formData(statistics)

            //get notifications from server
            val notifications = Sender.sendStatistics(dataToSend, state.apiKey, team, setTypes())
            if (notifications != null) {

                //save this data to check next time if need to send statistics to server
                statistics.sentTimePerDay = statistics.timePerDay
                statistics.sentMaxContinuousTimePerDay = statistics.maxContinuousTimePerDay

                NotificationUtils.getInstance().showNotifications(notifications)
            }
        }
    }

    private fun setTypes(): List<AchievementType>? {
        if (!state.enabledAllNotifications) {
            return null
        }

        val types = mutableListOf<AchievementType>()
        if (state.enabledLevelAchievementNotifications) {
            types.add(AchievementType.LEVEL)
        }
        if (state.enabledExtraAchievementNotifications) {
            types.add(AchievementType.EXTRA)
        }
        if (state.enabledTeamAchievementNotifications) {
            types.add(AchievementType.TEAM)
        }
        return if (types.isEmpty()) null else types
    }

    private fun needToSend(entry: MutableMap.MutableEntry<Team, Statistics>): Boolean {
        val statistics = entry.value
        return statistics.timePerDay > statistics.sentTimePerDay ||
                statistics.maxContinuousTimePerDay > statistics.sentMaxContinuousTimePerDay ||
                (TimeUtils.getInstance().changedContinuous(statistics))
    }

    /**
     * Adds statistics to [dataToSend]
     */
    private fun formData(statistics: Statistics) {
        dataToSend =
                listOf(
                        StatisticsDto(type = StatisticsType.TIME, value = statistics.timePerDay, subType = StatisticsSubType.PER_DAY),
                        StatisticsDto(type = StatisticsType.TIME, value = statistics.timePerLife, subType = StatisticsSubType.PER_LIFE),
                        StatisticsDto(type = StatisticsType.SYMBOL, value = statistics.symbolsQuantityPerDay, subType = StatisticsSubType.PER_DAY),
                        StatisticsDto(type = StatisticsType.SYMBOL, value = statistics.symbolsQuantityPerLife, subType = StatisticsSubType.PER_LIFE),
                        StatisticsDto(type = StatisticsType.COMMIT, value = statistics.successfulCommitsPerLife, subType = StatisticsSubType.PER_LIFE, extraValue = Constants.SUCCESSFUL),
                        StatisticsDto(type = StatisticsType.COMMIT, value = statistics.failedCommitsPerLife, subType = StatisticsSubType.PER_LIFE, extraValue = Constants.FAILED),
                        StatisticsDto(type = StatisticsType.COMMIT, value = statistics.successfulCommitsPerDay, subType = StatisticsSubType.PER_DAY, extraValue = Constants.SUCCESSFUL),
                        StatisticsDto(type = StatisticsType.COMMIT, value = statistics.failedCommitsPerDay, subType = StatisticsSubType.PER_DAY, extraValue = Constants.FAILED),
                        StatisticsDto(type = StatisticsType.EXEC, value = statistics.successfulExecutionsPerLife, subType = StatisticsSubType.PER_LIFE, extraValue = Constants.SUCCESSFUL),
                        StatisticsDto(type = StatisticsType.EXEC, value = statistics.failedExecutionsPerLife, subType = StatisticsSubType.PER_LIFE, extraValue = Constants.FAILED),
                        StatisticsDto(type = StatisticsType.EXEC, value = statistics.successfulExecutionsPerDay, subType = StatisticsSubType.PER_DAY, extraValue = Constants.SUCCESSFUL),
                        StatisticsDto(type = StatisticsType.EXEC, value = statistics.failedExecutionsPerDay, subType = StatisticsSubType.PER_DAY, extraValue = Constants.FAILED),
                        StatisticsDto(type = StatisticsType.TIME, value = continuous[ContinuousType.TIME_PER_DAY]!!, subType = StatisticsSubType.CONTINUOUS_PER_DAY),
                        StatisticsDto(type = StatisticsType.TIME, value = continuous[ContinuousType.TIME_PER_LIFE]!!, subType = StatisticsSubType.CONTINUOUS_PER_LIFE),
                        StatisticsDto(type = StatisticsType.SYMBOL, value = continuous[ContinuousType.SYMBOLS_PER_DAY]!!, subType = StatisticsSubType.CONTINUOUS_PER_DAY),
                        StatisticsDto(type = StatisticsType.SYMBOL, value = continuous[ContinuousType.SYMBOLS_PER_LIFE]!!, subType = StatisticsSubType.CONTINUOUS_PER_LIFE)

                ).toMutableList()


        //to much info - need to send only not 0 values
        //keys
        statistics.keyTapQuantity.forEach { keyName, quantity ->
            if (quantity.perLife != 0L) {
                dataToSend.add(StatisticsDto(type = StatisticsType.SINGLE_KEY, value = quantity.perLife,
                        subType = StatisticsSubType.PER_LIFE, extraValue = keyName))
            }
            if (quantity.perDay != 0L) {
                dataToSend.add(StatisticsDto(type = StatisticsType.SINGLE_KEY, value = quantity.perDay,
                        subType = StatisticsSubType.PER_DAY, extraValue = keyName))
            }
        }

        //languages
        statistics.languageInfo.forEach { langName, langInfo ->
            if (langInfo.timePerDay != 0L) {
                dataToSend.add(StatisticsDto(type = StatisticsType.LANG_TIME, value = langInfo.timePerDay,
                        subType = StatisticsSubType.PER_DAY, extraValue = langName))
            }

            if (langInfo.timePerLife != 0L) {
                dataToSend.add(StatisticsDto(type = StatisticsType.LANG_TIME, value = langInfo.timePerLife,
                        subType = StatisticsSubType.PER_LIFE, extraValue = langName))
            }

            if (langInfo.symbolsPerDay != 0L) {
                dataToSend.add(StatisticsDto(type = StatisticsType.LANG_SYMBOL, value = langInfo.symbolsPerDay,
                        subType = StatisticsSubType.PER_DAY, extraValue = langName))
            }

            if (langInfo.symbolsPerLife != 0L) {
                dataToSend.add(StatisticsDto(type = StatisticsType.LANG_SYMBOL, value = langInfo.symbolsPerLife,
                        subType = StatisticsSubType.PER_LIFE, extraValue = langName))
            }
        }
    }
}
