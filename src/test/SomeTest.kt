
import com.intellij.testFramework.LightPlatformTestCase.getProject
import com.intellij.codeInsight.actions.OptimizeImportsAction.actionPerformedImpl
import com.intellij.codeInsight.generation.actions.CommentByLineCommentAction
import com.intellij.openapi.fileTypes.FileType
import com.intellij.testFramework.fixtures.LightCodeInsightFixtureTestCase

class SomeTest : LightCodeInsightFixtureTestCase() {

    fun testCommenter() {
        myFixture.configureByText(SimpleFileType., "<caret>website = http://en.wikipedia.org/")
        val commentAction = CommentByLineCommentAction()
        commentAction.actionPerformedImpl(project, myFixture.getEditor())
        myFixture.checkResult("#website = http://en.wikipedia.org/")
        commentAction.actionPerformedImpl(project, myFixture.getEditor())
        myFixture.checkResult("website = http://en.wikipedia.org/")
    }
}