# NoIdle plugin
for [NoIdle server](https://github.com/endovitskayaV/noidle_server)

**[Download plugin (.jar)](https://gitlab.com/endovitskayaV/NoIdle/releases)**


## Installation
1. [Download plugin (.jar)](https://gitlab.com/endovitskayaV/NoIdle/releases)
2. Intellij IDEA: File -> Settings -> Plugins -> Install plugin from disk<br/>
   Choose *noidle.jar* location, click OK and restart Intellij IDEA. <br/> 
   <details>
    <summary>For more details <i>see installation screenshot</i> or [follow official guide](https://www.jetbrains.com/help/idea/managing-plugins.html)</summary>
    <p><br><img src="https://gitlab.com/endovitskayaV/NoIdle/raw/screenshots/installation.png" height="400"></p>
   </details>
   
   

## Usage
1. After restarting Intellij IDEA you will be asked to set API key. <br/>
   To get API key visit [cloud server](https://noidle.herokuapp.com) or [your custom server](https://github.com/endovitskayaV/noidle_server/tree/custom_server),
   register and copy API key from profile.<br/>
   If this dialog was not shown, set API key in File -> Settings -> Other Settings -> NoIdle 
   <details>
    <summary><i>Screenshots</i></summary>
    <p><br><img src="https://gitlab.com/endovitskayaV/NoIdle/raw/screenshots/apikey_dialog.png" height="100"></p>
    <p><br><img src="https://gitlab.com/endovitskayaV/NoIdle/raw/screenshots/register.png" height="350"></p>
    <p><br><img src="https://gitlab.com/endovitskayaV/NoIdle/raw/screenshots/profile.png" height="400"></p>
   </details><br/>

2. Set plugin preferences in File -> Setting -> Other Settings -> NoIdle<br/>
   <details>
    <summary><i>Settings screenshot</i></summary>
    <p><br><img src="https://gitlab.com/endovitskayaV/NoIdle/raw/screenshots/settings.png" height="350"></p>
   </details><br/>
3. Track your statistics in Tool Window<br/>
   <details>
    <summary><i>Tool Window screenshot</i></summary>
    <p><br><img src="https://gitlab.com/endovitskayaV/NoIdle/raw/screenshots/tool_window.png" height="500"></p>
   </details><br/>
4. Get informed about achievements by notifications<br/>
   <details>
    <summary><i>Notification screenshot</i></summary>
    <p><br><img src="https://gitlab.com/endovitskayaV/NoIdle/raw/screenshots/notifications.png" height="70"></p>
   </details><br/>


## Definitions
*API key* allows plugin to interact with server<br/><br>


## Troubleshooting
### Invalid team
1. Check internet access
2. Check team name spelling
3. Check if you are added to team with such name
4. Probably team with such name was deleleted and created again, so it has different id.<br/>
   To fix this, leave team name emty and save changes, then enter name again.<br/><br/>

Contact <a href="mailto:evvEndovitskaya@yandex.ru">evvEndovitskaya@yandex.ru</a><br/><br/>

## Important notes
1. You are free not to set API key if you want to get only daily reports,<br />
   but in this case you will not be able to get achievements and synchronize statistics.
2. Statistics is saved locally in *<home>/.<ide_name_version>/config/plugins* you can edit it, but this is against plugin aim.


  
 